﻿using System;
using System.Collections.Generic;
using GrozaSModelsDBLib;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using UISuppressSource;
using System.IO;
using System.Linq;

namespace GrozaS_AWS
{
    public partial class MainWindow
    {
        public void InitJammingControl()
        {
            JammingDrone.OnDoubleClickRange += JammingDrone_OnDoubleClickRange;
            JammingDrone.OnChangeRecord += JammingDrone_OnChangeRecord; 
            JammingDrone.GnssStateChanged += JammingDrone_GnssStateChanged;
        }


        public void JammingDrone_InitDroneTypeDictionary(Dictionary<byte, string> types)
        {
            JammingDrone.InitTypes(types);
        }


        private async void JammingDrone_LoadSuppressSources()
        {
            try
            {
                var tableJamming = await mainWindowViewModel.clientDB.Tables[NameTable.TableSuppressSource].LoadAsync<TableSuppressSource>();
                JammingDrone_UpdateSuppressSources(tableJamming);
            }
            catch { }
        }


        private async void JammingDrone_LoadSuppressGnss()
        {
            try
            {
                var tableJamming = await mainWindowViewModel.clientDB.Tables[NameTable.TableSuppressGnss].LoadAsync<TableSuppressGnss>();
                JammingDrone_UpdateSuppressGnss(tableJamming);
            }
            catch { }
        }
    }
}
