﻿using System;
using System.Collections.Generic;
using GrozaSModelsDBLib;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using UISuppressSource;
using System.IO;
using System.Linq;
using System.Threading;

namespace GrozaS_AWS
{
    public partial class MainWindow
    {

        private void JammingDrone_UpdateSuppressSources(List<TableSuppressSource> data)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                foreach (TableSuppressSource record in data)
                {
                    JammingDrone.SetSuppressSource(record);
                }
            });
        }


        private void JammingDrone_UpdateSuppressGnss(List<TableSuppressGnss> data)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                foreach (TableSuppressGnss record in data)
                {
                    JammingDrone.SetSuppressGnss(record);
                }
            });
        }


        private void JammingDrone_OnLocalPropChanged()
        {
            if (mainWindowViewModel.clientDB != null && mainWindowViewModel.clientDB.IsConnected())
            {
                JammingDrone_LoadSuppressSources();
            }
        }


        #region Control EventHandlers

        private void JammingDrone_OnChangeRecord(object sender, TableSuppressSource e)
        {
            if (mainWindowViewModel.clientDB == null)
                return;

            mainWindowViewModel.clientDB?.Tables[NameTable.TableSuppressSource].ChangeAsync(e);
        }


        private void JammingDrone_GnssStateChanged(object sender, TableSuppressGnss e)
        {
            if (mainWindowViewModel.clientDB == null)
                return;

            mainWindowViewModel.clientDB?.Tables[NameTable.TableSuppressGnss].ChangeAsync(e);
        }


        private void JammingDrone_OnDoubleClickRange(object sender, TableSuppressSource e)
        {
            var findDrone = CurrentSourcesDF.FirstOrDefault(x => x.ID == e.TableSourceId);
            if (findDrone == null)
                return;

            Coord coordCenter = findDrone.Track.Last().Coordinate;
            mainWindowViewModel.LocationCenter = coordCenter;
        }
        #endregion
    }
}
