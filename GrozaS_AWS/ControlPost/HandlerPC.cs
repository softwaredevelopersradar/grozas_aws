﻿
using ClientDataBase.ServiceDB;
using InheritorsEventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using ClientDataBase;
using GrozaSModelsDBLib;
using System.Threading;

using UIMap;
using AutoMapper;
using System.Windows.Threading;
using WPFControlConnection;
using WpfMapControl;
using CoordFormatLib;
using TableEvents;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {

        private void CPConnection_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (clientBerezinaPC != null)
                    DisconnectBerezina();
                else

                    ConnectBerezina();
            }
            catch
            { }
        }


        private void ClientBerezinaPC_OnReceiveConfirmTextCmd(object sender, byte bCodeError)
        {
            try
            {
                newWindow.ConfirmSentMessage(0);
            }
            catch
            { }
        }

        private void ClientBerezinaPC_OnReceiveTextCmd(object sender, string strText)
        {
            try
            {                
                chatBuble.SetMessage(strText);

                newWindow.DrawReceivedMessage(0, strText);

                if (clientBerezinaPC != null)
                    clientBerezinaPC.SendConfirmText(0);
            }
            catch
            {

            }
        }

        private void ClientBerezinaPC_OnRequestSynchTime(object sender, StructTypeGR_BRZ.TTimeMy tTime)
        {
            
            DateTime dateTimePC = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, tTime.bHour, tTime.bMinute, tTime.bSecond);


            var bCodeError = (SynchTimePC(dateTimePC) == false) ? 0 : 1;


            StructTypeGR_BRZ.TTimeMy tTimeMy = new StructTypeGR_BRZ.TTimeMy();

            tTimeMy.bHour = Convert.ToByte(DateTime.Now.Hour);
            tTimeMy.bMinute = Convert.ToByte(DateTime.Now.Minute);
            tTimeMy.bSecond = Convert.ToByte(DateTime.Now.Second);


            bool SynchTimePC(DateTime dtDateTimeGNSS)
            {

                try
                {
                    SYSTEMTIME st = new SYSTEMTIME();
                    st.FromDateTime(dtDateTimeGNSS);


                    //Call Win32 API to set time
                    SetLocalTime(ref st);

                    return true;
                }

                catch
                {
                    return false;
                }
            }


            if (clientBerezinaPC != null)
                clientBerezinaPC.SendSynchTime((byte)bCodeError, tTimeMy);
          
        }

        private void ClientBerezinaPC_OnRequestSupprFWS(object sender, int iDuration, StructTypeGR_BRZ.TSupprFWS[] tSupprFWS)
        {
            // add data to table

            if (clientBerezinaPC != null)
                clientBerezinaPC.SendSupprFWS(1);
        }

            private void ClientBerezinaPC_OnRequestSupprFHSS(object sender, int iDuration, StructTypeGR_BRZ.TSupprFHSS[] tSupprFHSS)
        {
            
            if (clientBerezinaPC != null)
                clientBerezinaPC.SendSupprFHSS(1);
        }

        private void ClientBerezinaPC_OnRequestStateSupprFWS(object sender)
        {
            if (clientBerezinaPC != null)
                clientBerezinaPC.SendStateSupprFWS(1, null);
        }

        private void ClientBerezinaPC_OnRequestStateSupprFHSS(object sender)
        {
            if (clientBerezinaPC != null)
                clientBerezinaPC.SendStateSupprFHSS(1, null);
        }

        private void ClientBerezinaPC_OnRequestState(object sender)
        {

            try
            {
                byte[] bLetter = new byte[9];
                clientBerezinaPC.SendState(0, (byte)mainWindowViewModel.TempMode, 
                                              (byte)lJammerStation.Find(x => x.Role == StationRole.Own).Id, 
                                              2, 0, bLetter);
            }
            catch { }

            
        }

        private void ClientBerezinaPC_OnRequestSimulBear(object sender, int iID, int iFreq)
        {
            if (clientBerezinaPC != null)
                clientBerezinaPC.SendSimulBear(1, iID, iFreq, 361, 361);
        }

        private void ClientBerezinaPC_OnRequestRegime(object sender, byte bRegime)
        {

            if (clientBerezinaPC != null)
                clientBerezinaPC.SendRegime((byte)mainWindowViewModel.TempMode);
        }

        private void ClientBerezinaPC_OnRequestReconFWS(object sender)
        {
            
            StructTypeGR_BRZ.TReconFWS[] tReconFWSRes = null;
            tReconFWSRes = GetCurrentSource();


            StructTypeGR_BRZ.TReconFWS[] GetCurrentSource()
            {
                StructTypeGR_BRZ.TReconFWS[] tReconFWS = null;

                try
                {
                    tReconFWS = new StructTypeGR_BRZ.TReconFWS[CurrentSourcesDF.Count];

                    var NumJammerOwn = lJammerStation.FirstOrDefault(x => x.Role == StationRole.Own).Id;


                    var NumJammerLink = -1;

                    try
                    {
                        NumJammerLink = lJammerStation.FirstOrDefault(x => x.Role == StationRole.Linked).Id;
                    }
                    catch { }

                    foreach (var cs in CurrentSourcesDF)
                    {
                        int i = CurrentSourcesDF.IndexOf(cs);

                        tReconFWS[i].bBandWidth = (byte)(cs.Track.Last().Band / 1000f);// (byte)(Math.Round((Decimal)((double)tSourceWork[i].iWidth / 1000.0)));                       
                        tReconFWS[i].iFreq = (int)(cs.Track.Last().Frequency *10);
                        tReconFWS[i].iID = cs.ID;
                        tReconFWS[i].sBearingLinked = (short)(cs.Track.Last().Bearings.Where(x => x.Jammer == NumJammerOwn).Last().Bearing);
                        
                        tReconFWS[i].sBearingOwn = 361;

                        try
                        {
                            tReconFWS[i].sBearingOwn = (short)(cs.Track.Last().Bearings.Where(x => x.Jammer == NumJammerLink).Last().Bearing);
                        }
                        catch { }
                       
                        tReconFWS[i].tTime.bHour = (byte)(cs.Track.Last().Time.Hour);
                        tReconFWS[i].tTime.bMinute = (byte)(cs.Track.Last().Time.Minute);
                        tReconFWS[i].tTime.bSecond = (byte)(cs.Track.Last().Time.Second);

                    }
                }
                catch
                { }
                               
                return tReconFWS;

            }

            if (clientBerezinaPC != null)
                clientBerezinaPC.SendReconFWS(0, tReconFWSRes);
            
            
        }

        private void ClientBerezinaPC_OnRequestReconFHSS(object sender)
        {

            if (clientBerezinaPC != null)
                clientBerezinaPC.SendStateSupprFHSS(1, null);                       
        }

        private void ClientBerezinaPC_OnRequestRangeSpec(object sender, byte bType, StructTypeGR_BRZ.TRangeSpec[] tRangeSpec)
        {
            if (tRangeSpec == null)
                return;

            List<FreqRanges> lFreq = new List<FreqRanges>();


            int JammerOwn = lJammerStation.Where(x => x.Role == StationRole.Own).FirstOrDefault().Id;
            for (int i = 0; i < tRangeSpec.Length; i++)
            {
                lFreq.Add(new FreqRanges()
                {
                    FreqMinKHz = (int)Math.Round((Decimal)((double)tRangeSpec[i].iFregMin / 10.0)),
                    FreqMaxKHz = (int)Math.Round((Decimal)((double)tRangeSpec[i].iFregMax / 10.0)),
                    IsActive = true,
                    NumberASP = JammerOwn
                });

            }


            switch (bType)
            {
                case 0:
                    List<TableFreqForbidden> tableFreqForbidden = new List<TableFreqForbidden>();

                    foreach (var lf in lFreq)
                        tableFreqForbidden.Add(lf.ToFreqForbidden());

                    UpdateDB_PC(NameTable.TableFreqForbidden, (object)tableFreqForbidden);

                    break;

                case 2:
                    List<TableFreqKnown> tableFreqKnown = new List<TableFreqKnown>();

                    foreach (var lf in lFreq)
                        tableFreqKnown.Add(lf.ToFreqKnown());

                    UpdateDB_PC(NameTable.TableFreqKnown, (object)tableFreqKnown);
                    break;

                default:
                    break;
            }


            if (clientBerezinaPC != null)
                clientBerezinaPC.SendRangeSpec(0, bType);


        }

        private void ClientBerezinaPC_OnRequestRangeSector(object sender, byte bType, StructTypeGR_BRZ.TRangeSector[] tRangeSector)
        {
            if (clientBerezinaPC != null)
                clientBerezinaPC.SendRangeSector(1, bType);

        }

        private void UpdateDB_PC(NameTable nameTable, object obj)
        {
            if (mainWindowViewModel.clientDB != null)
            {

                mainWindowViewModel.clientDB?.Tables[nameTable].CLearAsync();

                mainWindowViewModel.clientDB?.Tables[nameTable].AddRangeAsync(obj);
            }
        }


        private void ClientBerezinaPC_OnRequestExecBear(object sender, int iID, int iFreq)
        {
            if (clientBerezinaPC != null)
                clientBerezinaPC.SendExecBear(1, iID, iFreq, 361);
          
        }

        private void ClientBerezinaPC_OnRequestCoord(object sender)
        {

            try
            {
                Coord coord = lJammerStation.Find(x => x.Role == StationRole.Own).Coordinates;
                VCFormat VCoord = new VCFormat(coord.Latitude, coord.Longitude);

                StructTypeGR_BRZ.TCoord tCoord = new StructTypeGR_BRZ.TCoord();

                tCoord.bSignLatitude = (byte)((coord.Latitude>0) ? 1 : 0);
                tCoord.bDegreeLatitude = (byte)VCoord.coordDegMinSec.Latitude.Degree;
                tCoord.bMinuteLatitude = (byte)VCoord.coordDegMinSec.Latitude.Minute;
                tCoord.bSecondLatitude = (byte)VCoord.coordDegMinSec.Latitude.Second;


                tCoord.bSignLongitude = (byte)((coord.Latitude > 0) ? 1 : 0);
                tCoord.bDegreeLongitude = (byte)VCoord.coordDegMinSec.Longitude.Degree;
                tCoord.bMinuteLongitude = (byte)VCoord.coordDegMinSec.Longitude.Minute;
                tCoord.bSecondLongitude = (byte)VCoord.coordDegMinSec.Longitude.Second;

                if (clientBerezinaPC != null)
                    clientBerezinaPC.SendCoord(0, tCoord);
            }
            catch { }

        }

        private void ClientBerezinaPC_OnDisconnect(object sender)
        {
            mainWindowViewModel.StateConnectionClientCP = ConnectionStates.Disconnected;
        }

        private void ClientBerezinaPC_OnConnect(object sender)
        {
            mainWindowViewModel.StateConnectionClientCP = ConnectionStates.Connected;
        }
    }
}