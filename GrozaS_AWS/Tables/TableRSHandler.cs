﻿using GrozaS_AWS.Models;
using GrozaSModelsDBLib;
using RecognitionSystemProtocol;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TableEvents;
using UIMap;

namespace GrozaS_AWS
{
    public partial class MainWindow
    {
        private async void UcOwnUAV_OnSendRecord(object sender, TableOwnUAV e)
        {
           
            UAV uav = new UAV();

            uav.ID = Encoding.ASCII.GetBytes(e.SerialNumber);
        
            uav.FreqMinMHz = e.Frequencies[0].FrequencyMHz;
            uav.BandwidthMHz = e.Frequencies[0].BandMHz;
            byte res = await SendAddOWnUAV(uav);

            
         
            switch (res)
            {
                case 0:
                    
                    break;

                case 1:
                    break;

                case 2:
                    break;

                case 3:
                    break;

                default:
                    break;


            }


            //lOwnUAV.Where(x => x.SerialNumber == e.SerialNumber).FirstOrDefault().Frequencies.Where(x => x.Id == e.Frequencies[0].Id).FirstOrDefault().SR = !Convert.ToBoolean(res);

            TableOwnUAV tableOwnUAV = new TableOwnUAV();

            tableOwnUAV = lOwnUAV.Where(x => x.SerialNumber == e.SerialNumber).FirstOrDefault();
            tableOwnUAV.Frequencies.Where(x => x.Id == e.Frequencies[0].Id).FirstOrDefault().SR = !Convert.ToBoolean(res);

            flagUpdateSR = true;

            OnChangeRecord(this, new TableEvent(tableOwnUAV));
        }

    }
}