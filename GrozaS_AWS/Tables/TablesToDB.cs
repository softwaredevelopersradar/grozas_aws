﻿using GrozaS_AWS.Models;
using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TableEvents;
using UIMap;

namespace GrozaS_AWS
{
    public partial class MainWindow
    {
        // Станции помех
        public List<TableJammerStation> lJammerStation = new List<TableJammerStation>();
        // Известные частоты (ИЧ)
        public List<FreqRanges> lFreqKnown = new List<FreqRanges>();
        // Диапазоны радиоразведки (ДРР)
        public List<FreqRanges> lFreqRangesRecon = new List<FreqRanges>();
        // Запрещенные частоты (ЗЧ)
        public List<FreqRanges> lFreqForbidden = new List<FreqRanges>();
        // Сектора радиоразведки (СРР)
        public List<TableSectorsRecon> lSectorsRecon = new List<TableSectorsRecon>();
        // Свои БПЛА 
        public List<TableOwnUAV> lOwnUAV = new List<TableOwnUAV>();

        bool flagUpdateSR = false;

        //public List<TableSource> lSource = new List<TableSource>();

        ObservableCollection<SourceDF> CurrentSourcesDF = new ObservableCollection<SourceDF>();

        ObservableCollection<VMJammer> JammersDB = new ObservableCollection<VMJammer>();


        // Добавить запись
        private void OnAddRecord(object sender, TableEvent e)
        {
            if (mainWindowViewModel.clientDB != null)
            {
                CheckCoordGNSS(ref e);

                mainWindowViewModel.clientDB.Tables[e.NameTable].Add(e.Record);
            }
        }

        // Удалить все записи
        private void OnClearRecords(object sender, NameTable nameTable)
        {
            if (mainWindowViewModel.clientDB != null)
            {
                ClearOwnUAV(nameTable);
                mainWindowViewModel.clientDB.Tables[nameTable].Clear();
            }
        }

        // Удалить запись
        private void OnDeleteRecord(object sender, TableEvent e)
        {
            if (mainWindowViewModel.clientDB != null)
            {
                DeleteOwnUAV(e);
                mainWindowViewModel.clientDB.Tables[e.NameTable].Delete(e.Record);
            }
        }

        // Изменить запись
        private void OnChangeRecord(object sender, TableEvent e)
        {
            if (mainWindowViewModel.clientDB != null)
            {
                CheckCoordGNSS(ref e);

                mainWindowViewModel.clientDB.Tables[e.NameTable].Change(e.Record);
            }
        }


        private void CheckCoordGNSS(ref TableEvent e)
        {
           
          
            if (e.NameTable == NameTable.TableJammerStation)
            {
                TableJammerStation tableJammerStation = e.Record as TableJammerStation;

                tableJammerStation.Coordinates = (tableJammerStation.IsGnssUsed) ?
                                                    mainWindowViewModel.GlobalPropertiesVM.Gnss : tableJammerStation.Coordinates;
               
            }


        }

        
        private void UcJammerStation_OnGetCoords(object sender, SelectedRowEvents e)
        {
            try
            {
                SendCoord(e.Id);
            }
            catch { }
        }

        private void UcJammerStation_OnGetTime(object sender, SelectedRowEvents e)
        {
            try
            {
                SendSynchTime(e.Id);
            }
            catch { }
        }

        private void UcJammerStation_OnSelectedRow(object sender, SelectedRowEvents e)
        {
            if (e.Id > 0)
            {
                LoadTablesByFilter(e.Id);
            }
        }
        private void UcJammerStation_OnDoubleClickStation(object sender, SelectedRowEvents e)
        {
            Coord coordCenter = JammersDB.Where(x => x.Id == e.Id).FirstOrDefault().Coordinates;

            mainWindowViewModel.LocationCenter = coordCenter;
        }

        

    }
}
