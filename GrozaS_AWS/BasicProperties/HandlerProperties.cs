﻿
using DllGrozaSProperties.Models;
using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Media;
using ValuesCorrectLib;

namespace GrozaS_AWS
{
    public partial class MainWindow
    {
        

        LocalProperties defaultLocalProperties = new LocalProperties();

        public void InitDefaultProperties()
        {
            defaultLocalProperties = YamlLoad(AppDomain.CurrentDomain.BaseDirectory + "DefaultLocalProperties.yaml");
           
        }


        public void InitLocalProperties()
        {
            mainWindowViewModel.LocalPropertiesVM.Map.PropertyChanged -= Map_PropertyChanged;
            basicProperties.Local.Lemt.PropertyChanged -= Lemt_PropertyChanged;


            mainWindowViewModel.LocalPropertiesVM = YamlLoad(AppDomain.CurrentDomain.BaseDirectory + "LocalProperties.yaml");
           
            LoadSignalFile();

            SetLocalProperties();

            if (mainWindowViewModel.LocalPropertiesVM != null)
            {
                mainWindowViewModel.LocalPropertiesVM.Map.PropertyChanged += Map_PropertyChanged;
                basicProperties.Local.Lemt.PropertyChanged += Lemt_PropertyChanged;
            }

        }

        private void Lemt_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {

            mainWindowViewModel.LocalPropertiesVM.Lemt.Human = basicProperties.Local.Lemt.Human;
            mainWindowViewModel.LocalPropertiesVM.Lemt.Car = basicProperties.Local.Lemt.Car;
            mainWindowViewModel.LocalPropertiesVM.Lemt.Helicopter = basicProperties.Local.Lemt.Helicopter;
            mainWindowViewModel.LocalPropertiesVM.Lemt.Group = basicProperties.Local.Lemt.Group;
            mainWindowViewModel.LocalPropertiesVM.Lemt.UAV = basicProperties.Local.Lemt.UAV;
            mainWindowViewModel.LocalPropertiesVM.Lemt.Tank = basicProperties.Local.Lemt.Tank;
            mainWindowViewModel.LocalPropertiesVM.Lemt.IFV = basicProperties.Local.Lemt.IFV;
            mainWindowViewModel.LocalPropertiesVM.Lemt.Unknown = basicProperties.Local.Lemt.Unknown;

            UpdateZorkiRTargetVisible();
        }

     
      

        #region HandlerLocalProperties

        public void HandlerLocalProperties(object sender, LocalProperties arg)
        {
            YamlSave(arg);

            InitLocalProperties();

            SetResourceLanguage(basicProperties.Local.General.Language);
            TranslatorTables.LoadDictionary(basicProperties.Local.General.Language);

            RastrMap.SetResourceLanguage((MapLanguages)basicProperties.Local.General.Language);
            newWindow.SetLanguage(basicProperties.Local.General.Language);

            SetViewMode();

            SaveSourceTDF_DB();

            //DroneControl_UpdateSourcesCollection();

            JammingDrone_OnLocalPropChanged();
            //ClientGnss_OnLocalPropChanged();
            StartGnssThreads();



            UpdateJammerMap(lJammerStation);
            LoadSignalFile();

            Property_CmpRX_Update();

            //Property_CmpTX_Update();

            UpdateErrorCmpTx();

            Property_Oem_Update();


            


        }

        private void UpdateErrorCmpTx()
        {
            mainWindowViewModel.DirectionTX.Error = mainWindowViewModel.LocalPropertiesVM.CmpTX.ErrorDeg;

            mainWindowViewModel.DirectionBRD.Error = mainWindowViewModel.LocalPropertiesVM.CmpTX.ErrorDeg;
        }

        private void LoadSignalFile()
        {
            try
            {
                playAlarm = new SoundPlayer(mainWindowViewModel.LocalPropertiesVM.General.AudioSignalFile);
            }
            catch
            { }
        }


        public void SetLocalProperties()
        {
            try
            {
                basicProperties.Local = mainWindowViewModel.LocalPropertiesVM;
            }
            catch { }
        }




        private void HandlerLocalDefaultProperties_Click(object sender, EventArgs e)
        {
            try
            {
                InitDefaultProperties();

                HandlerLocalProperties(this, defaultLocalProperties);

                SetLocalProperties();
            }
            catch
            { }
        }
        #endregion


        #region HandlerGlobalProperties
        public void HandlerGlobalProperties(object sender, DllGrozaSProperties.Models.GlobalProperties arg)
        {

            GrozaSModelsDBLib.GlobalProperties globalPropertiesChange = iMapper.Map<DllGrozaSProperties.Models.GlobalProperties, GrozaSModelsDBLib.GlobalProperties>(arg);
            
            UpdateGlobalProperties(globalPropertiesChange);

            SaveSourceTDF_DB();
        }


        public void UpdateGlobalProperties(GrozaSModelsDBLib.GlobalProperties globalPropertiesChange)
        {
            globalPropertiesChange.Id = 1;

            try
            {
                mainWindowViewModel.clientDB?.Tables[NameTable.GlobalProperties].Add(globalPropertiesChange);
            }

            catch
            { }

            
        }


        public void SetGlobalProperties()
        {
            try
            {
                iMapper.Map(mainWindowViewModel.GlobalPropertiesVM, basicProperties.Global);
            }
            catch
            { }
        }

        private void HandlerGlobalDefaultProperties_Click(object sender, EventArgs e)
        {
            try
            {
                mainWindowViewModel.clientDB?.Tables[NameTable.GlobalProperties].Add(new GrozaSModelsDBLib.GlobalProperties() { Id = 1 });
            }

            catch
            { }
        }


        private void UpdateZorkiRTargetVisible()
        {
            Dictionary<ViewZorkiR, bool> dictTemp = new Dictionary<ViewZorkiR, bool>();
            foreach (var zrv in MainWindowViewModel.GetEnumValues<ViewZorkiR>())
                dictTemp.Add(zrv, true);


            dictTemp[ViewZorkiR.Human] = mainWindowViewModel.LocalPropertiesVM.Lemt.Human;
            dictTemp[ViewZorkiR.Car] = mainWindowViewModel.LocalPropertiesVM.Lemt.Car;
            dictTemp[ViewZorkiR.Helicopter] = mainWindowViewModel.LocalPropertiesVM.Lemt.Helicopter;
            dictTemp[ViewZorkiR.Group] = mainWindowViewModel.LocalPropertiesVM.Lemt.Group;
            dictTemp[ViewZorkiR.UAV] = mainWindowViewModel.LocalPropertiesVM.Lemt.UAV;
            dictTemp[ViewZorkiR.Tank] = mainWindowViewModel.LocalPropertiesVM.Lemt.Tank;
            dictTemp[ViewZorkiR.IFV] = mainWindowViewModel.LocalPropertiesVM.Lemt.IFV;
            dictTemp[ViewZorkiR.Unknown] = mainWindowViewModel.LocalPropertiesVM.Lemt.Unknown;

            mainWindowViewModel.ZorkiRTargetVisible = dictTemp;
        }


        private void CheckEmptySpoofPoint()
        {
            if ((mainWindowViewModel.GlobalPropertiesVM.Spoofing.Latitude == -1 || mainWindowViewModel.GlobalPropertiesVM.Spoofing.Longitude == -1) && (mainWindowViewModel.GlobalPropertiesVM.Gnss.Latitude != -1 && mainWindowViewModel.GlobalPropertiesVM.Gnss.Longitude != -1))
            {
                mainWindowViewModel.GlobalPropertiesVM.Spoofing = new Coord() { Latitude = mainWindowViewModel.GlobalPropertiesVM.Gnss.Latitude, Longitude = mainWindowViewModel.GlobalPropertiesVM.Gnss.Longitude  }; 
            }

        }
        #endregion
    }
}