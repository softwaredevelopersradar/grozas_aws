﻿using GrozaS_AWS.TDF;
using System.Windows;
using TDF;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        public TCPClientTDF tcpClientTDF;

        Range RangeRI_1_2 = new Range(1200, 1280);
        Range RangeRI_2_4 = new Range(2400, 2480);
        Range RangeRI_5_8 = new Range(5725, 5850);

        private void ConnectTDF()
        {
            if (tcpClientTDF != null)
                DisconnectTDF();

            try
            {
                tcpClientTDF = new TCPClientTDF();

                tcpClientTDF.OnDestroy += DestroyServerDF;
                tcpClientTDF.OnConnect += ConnectClientDF;
                tcpClientTDF.OnDisconnect += DisconnectClientDF;

                tcpClientTDF.OnBearing += BearingTDF;

                tcpClientTDF.OnBearingDistance += BearingDistanceTDF;

                tcpClientTDF.OnChannel += ChannelTDF;

                tcpClientTDF.OnCSource += CSourceTDF;

                tcpClientTDF.OnSource += SourceTDF;

                tcpClientTDF.OnSourceDistance += SourceDistanceTDF;

                tcpClientTDF.OnSourceDistanceFRX += SourceDistanceFRXTDF;

                tcpClientTDF.OnSourceDistanceSignal += SourceDistanceSignalTDF;

                tcpClientTDF.OnRegime += RegimeTDF;

                tcpClientTDF.Connect(mainWindowViewModel.LocalPropertiesVM.DF.IpAddress, mainWindowViewModel.LocalPropertiesVM.DF.Port);
            }
            catch { }
        }

        private void DisconnectTDF()
        {
            if (tcpClientTDF != null)
            {
                tcpClientTDF.Disconnect();

                tcpClientTDF.OnDestroy -= DestroyServerDF;
                tcpClientTDF.OnConnect -= ConnectClientDF;
                tcpClientTDF.OnDisconnect -= DisconnectClientDF;

                tcpClientTDF.OnBearing -= BearingTDF;

                tcpClientTDF.OnChannel -= ChannelTDF;

                tcpClientTDF.OnCSource -= CSourceTDF;

                tcpClientTDF.OnSource -= SourceTDF;

                tcpClientTDF.OnRegime -= RegimeTDF;

                tcpClientTDF = null;
            }

        }

    }
}
