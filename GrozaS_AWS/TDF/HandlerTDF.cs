﻿using Bearing;
using GrozaSModelsDBLib;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using TDF;
using TDF.Events;
using UIMap;
using WPFControlConnection;



using System.Collections.Generic;
using GrozaS_AWS.Models;
using System.Linq.Expressions;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        private void TDFControlConnection_ButServerClick(object sender, RoutedEventArgs e)
        {
            switch (mainWindowViewModel.LocalPropertiesVM.DF.Type)
            {
                case DllGrozaSProperties.Models.TypeDF.ZhSV:
                    if (tcpClientTDF != null && tcpClientTDF.IsConnected)
                        DisconnectTDF();
                    else
                        ConnectTDF();
                    break;

                case DllGrozaSProperties.Models.TypeDF.ShIV:
                    if (tcpClientTDF != null && tcpClientTDF.IsConnected)
                        DisconnectDDF();
                    else
                        ConnectDDF();
                    break;
                
            }
            

        }



        #region ConnectionTDF

        private void DestroyServerDF(object sender, EventArgs e)
        {
            mainWindowViewModel.StateConnectionTDF = ConnectionStates.Disconnected;
        }

        private void ConnectClientDF(object sender, EventArgs e)
        {
            mainWindowViewModel.StateConnectionTDF = ConnectionStates.Connected;

            try
            {

                SendMode(Mode.STOP);
              
                if (mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle > -1)
                    SendCourseAngle((short)mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle);
            }
            catch
            { }
        }

        private void DisconnectClientDF(object sender, EventArgs e)
        {
            mainWindowViewModel.StateConnectionTDF = ConnectionStates.Disconnected;
        }

        #endregion




        #region Function Analize


        private bool CheckExist(double newFrequency)
        {

            double lastFrequency = CurrentSourcesDF.Last().Track.Last().Frequency;


            if ((RangeRI_1_2.Contains(lastFrequency) && RangeRI_1_2.Contains(newFrequency)) ||
                (RangeRI_2_4.Contains(lastFrequency) && RangeRI_2_4.Contains(newFrequency)) ||
                (RangeRI_5_8.Contains(lastFrequency) && RangeRI_5_8.Contains(newFrequency)))
                return true;
            else
                return (Math.Abs(lastFrequency - newFrequency) <= mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.FrequencyAccuracy);

        }


        private void DetermineSourceTDF(TableSource source)
        {
            //source.Type = IdentifyUAV(new TrackPoint() {Frequency = source.Track.Last().FrequencyMHz, Band = source.Track.Last().BandMHz }  );

            if (mainWindowViewModel.AutoUpdateSource && CheckExist(source.Track.Last().FrequencyMHz))
            {

                CurrentSourcesDF.Last().Type = source.Type;

                AddTrackSourceTDF(source.Track.Last());

                var OwnJammer = lJammerStation.Find(x => x.Role == StationRole.Own).Id;
                SetAngleOEM(source.Track.Last().Bearing.Where(x => x.NumJammer == OwnJammer).FirstOrDefault().Bearing);

                if (mainWindowViewModel.TempMode == Mode.JAMMING)
                    SetAngleBRD(source.Track.Last().Bearing.Where(x => x.NumJammer == OwnJammer).FirstOrDefault().Bearing);
            }

            else

                AddSourceTDF_DB(source);

        }





        private void AddTrackSourceTDF(TableTrack tableTrack)
        {
            
            try
            {
                ObservableCollection<BearingItem> BearingsAdd = new ObservableCollection<BearingItem>();
                foreach (var b in tableTrack.Bearing)
                    BearingsAdd.Add(new BearingItem()
                    {
                        Bearing = b.Bearing,
                        Distance = b.Distance,
                        Jammer = b.NumJammer
                    });


                CurrentSourcesDF.Last().AddTrackPoint(new TrackPoint()
                {
                    
                    Frequency = tableTrack.FrequencyMHz,
                    Band = tableTrack.BandMHz,
                    Coordinate = new Coord(),
                    Jammers = new ObservableCollection<TableJammerStation>(lJammerStation),
                    Bearings = BearingsAdd
                });


                //CurrentSourcesDF.Last().Type = IdentifyUAV(CurrentSourcesDF.Last().Track.Last());

                lastSource = CurrentSourcesDF.Last();

            }
            catch
            { }

        }

        private void UpdateTrackSourceTDF(BearingItem bearingItem)
        {
            try
            {

                CurrentSourcesDF.Last().UpdateTrackPoint(bearingItem);

            }
            catch
            { }

        }

        private void UpdateTrackSourceTDF(int IDSource, BearingItem bearingItem)
        {
            try
            {

                CurrentSourcesDF.Where(x=>x.ID == IDSource).FirstOrDefault().UpdateTrackPoint(bearingItem);

            }
            catch
            { }

        }





        #endregion


    }
}