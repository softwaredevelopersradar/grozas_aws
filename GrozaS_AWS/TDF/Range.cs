﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.TDF
{
    public class Range
    {
        private double MinValue { get; set; }
        private double MaxValue { get; set; }

        public Range(double minValue, double maxValue)
        {
            MinValue = minValue;
            MaxValue = maxValue;
        }

        public bool Contains(double Value)
        {
            return (Value >= MinValue && Value <= MaxValue);
            
        }

    }
}
