﻿using BRD;
using ClientDataBase;
using DLL_Compass;
using DllGrozaSProperties.Models;
using GrozaS_AWS.AddPanel;
using GrozaS_AWS.Models;
using GrozaSModelsDBLib;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;

using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WPFControlConnection;
using WpfMapControl;



namespace GrozaS_AWS
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public ClientDB clientDB;

        MarkSizeWnd markSizeWnd;

        

        private LocalProperties _localPropertiesVM;
        public LocalProperties LocalPropertiesVM
        {
            get { return _localPropertiesVM; }
            set
            {
                if (_localPropertiesVM != value)
                {
                    _localPropertiesVM = value;
                    OnPropertyChanged();

                }
            }
        }


       



        private GrozaSModelsDBLib.GlobalProperties _globalPropertiesVM;
        public GrozaSModelsDBLib.GlobalProperties GlobalPropertiesVM
        {
            get { return _globalPropertiesVM; }
            set
            {
                if (_globalPropertiesVM != value)
                {
                    _globalPropertiesVM = value;
                    OnPropertyChanged();

                }
            }
        }

        public SizeValue sizeDPSetting { get; set; }
        public SizeValue sizeDPTables { get; set; }
        public SizeValue sizeDPJamming { get; set; }
        public SizeValue sizeDPSpoofing { get; set; }


        private ConnectionStates _stateConnectionTDF = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionTDF
        {
            get { return _stateConnectionTDF; }
            set
            {
                if (_stateConnectionTDF != value)
                {
                    _stateConnectionTDF = value;
                    OnPropertyChanged();

                }
            }
        }


        private ConnectionStates _stateConnectionGNSS = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionGNSS
        {
            get { return _stateConnectionGNSS; }
            set
            {
                if (_stateConnectionGNSS != value)
                {
                    _stateConnectionGNSS = value;
                    OnPropertyChanged();

                }
            }
        }


        private ConnectionStates _stateConnectionOEM = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionOEM
        {
            get { return _stateConnectionOEM; }
            set
            {
                if (_stateConnectionOEM != value)
                {
                    _stateConnectionOEM = value;
                    OnPropertyChanged();

                }
            }
        }

        private ConnectionStates _stateConnectionDB = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionDB
        {
            get { return _stateConnectionDB; }
            set
            {
                if (_stateConnectionDB != value)
                {
                    _stateConnectionDB = value;
                    OnPropertyChanged();

                }
            }
        }


        private ConnectionStates _stateConnectionCmpRX = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionCmpRX
        {
            get { return _stateConnectionCmpRX; }
            set
            {
                if (_stateConnectionCmpRX != value)
                {
                    _stateConnectionCmpRX = value;
                    OnPropertyChanged();

                }
            }
        }


        private ConnectionStates _stateConnectionCmpTX = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionCmpTX
        {
            get { return _stateConnectionCmpTX; }
            set
            {
                if (_stateConnectionCmpTX != value)
                {
                    _stateConnectionCmpTX = value;
                    OnPropertyChanged();

                }
            }
        }


        private ConnectionStates _stateConnectionBRD = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionBRD
        {
            get { return _stateConnectionBRD; }
            set
            {
                if (_stateConnectionBRD != value)
                {
                    _stateConnectionBRD = value;
                    OnPropertyChanged();

                }
            }
        }

        private ConnectionStates _stateConnectionLNK = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionLNK
        {
            get { return _stateConnectionLNK; }
            set
            {
                if (_stateConnectionLNK != value)
                {
                    _stateConnectionLNK = value;
                    OnPropertyChanged();

                }
            }
        }

        private ConnectionStates _stateConnectionServerCL = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionServerCL
        {
            get { return _stateConnectionServerCL; }
            set
            {
                if (_stateConnectionServerCL != value)
                {
                    _stateConnectionServerCL = value;
                    OnPropertyChanged();

                }
            }
        }

        private ConnectionStates _stateConnectionClientCL = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionClientCL
        {
            get { return _stateConnectionClientCL; }
            set
            {
                if (_stateConnectionClientCL != value)
                {
                    _stateConnectionClientCL = value;
                    OnPropertyChanged();

                }
            }
        }


        private ConnectionStates _stateConnectionED = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionED
        {
            get { return _stateConnectionED; }
            set
            {
                if (_stateConnectionED != value)
                {
                    _stateConnectionED = value;
                    OnPropertyChanged();

                }
            }
        }

        private ConnectionStates _stateConnectionRodnik = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionRodnik
        {
            get { return _stateConnectionRodnik; }
            set
            {
                if (_stateConnectionRodnik != value)
                {
                    _stateConnectionRodnik = value;
                    OnPropertyChanged();

                }
            }
        }



        private ConnectionStates _stateConnectionZorkiR = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionZorkiR
        {
            get { return _stateConnectionZorkiR; }
            set
            {
                if (_stateConnectionZorkiR != value)
                {
                    _stateConnectionZorkiR = value;
                    OnPropertyChanged();

                }
            }
        }

        private ConnectionStates _stateConnectionRS = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionRS
        {
            get { return _stateConnectionRS; }
            set
            {
                if (_stateConnectionRS != value)
                {
                    _stateConnectionRS = value;
                    OnPropertyChanged();

                }
            }
        }


        private ConnectionStates _stateConnectionClientCP = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionClientCP
        {
            get { return _stateConnectionClientCP; }
            set
            {
                if (_stateConnectionClientCP != value)
                {
                    _stateConnectionClientCP = value;
                    OnPropertyChanged();

                }
            }
        }

        private Direction _directionRX;

        public Direction DirectionRX
        {
            get { return _directionRX; }
            set
            {
                if (_directionRX != value)
                {
                    _directionRX = value;
                    OnPropertyChanged();

                }
            }
        }

        private Direction _directionTX;

        public Direction DirectionTX
        {
            get { return _directionTX; }
            set
            {
                if (_directionTX != value)
                {
                    _directionTX = value;
                    OnPropertyChanged();

                }
            }
        }

        private Direction _directionOEM;

        public Direction DirectionOEM
        {
            get { return _directionOEM; }
            set
            {
                if (_directionOEM != value)
                {
                    _directionOEM = value;
                    OnPropertyChanged();

                }
            }
        }

        private Direction _directionBRD;

        public Direction DirectionBRD
        {
            get { return _directionBRD; }
            set
            {
                if (_directionBRD != value)
                {
                    _directionBRD = value;
                    OnPropertyChanged();

                }
            }
        }


        private Coord _locationCenter;

        public Coord LocationCenter
        {
            get { return _locationCenter; }
            set
            {
                
                _locationCenter = value;
                OnPropertyChanged();


            }
        }

      


        private bool _autoUpdateSource = false;

        public bool AutoUpdateSource
        {
            get { return _autoUpdateSource; }
            set
            {
                _autoUpdateSource = value;
                OnPropertyChanged();
            }
        }


        private Mode _tempMode = Mode.STOP;

        public Mode TempMode
        {
            get { return _tempMode; }
            set
            {
                _tempMode = value;
                OnPropertyChanged();
            }
        }

        private string _viewMode = "";

        public string ViewMode
        {
            get { return _viewMode; }
            set
            {
                _viewMode = value;
                OnPropertyChanged();
            }
        }


        private bool _autoMode= false;

        public bool AutoMode
        {
            get { return _autoMode; }
            set
            {
                _autoMode = value;
                OnPropertyChanged();
            }
        }

        public Direction SetAngleBRD(float AngleNeed, Direction DirectionInput)
        {            
            var dif = (AngleNeed > DirectionInput.Result) ? (AngleNeed - DirectionInput.Result) : (360 - (DirectionInput.Result - AngleNeed));

            float val = DirectionInput.Azimuth + dif;
            while (val > 360)
                val -= 360;

            Direction DirectionOut = new Direction() { Error = DirectionInput.Error, CourseAngle = DirectionInput.CourseAngle, Azimuth = val };
            return DirectionOut;

        }

        public Direction SetAngleOEM(float AngleNeed, Direction DirectionInput)
        {            
            Direction DirectionOut = new Direction() { Error = DirectionInput.Error*(-1), CourseAngle = DirectionInput.CourseAngle*(-1), Azimuth = AngleNeed };
            return DirectionOut;

        }


        


        private ExBearing _exBearingSendRequest = new ExBearing();

        public ExBearing ExBearingSendRequest
        {
            get { return _exBearingSendRequest; }
            set
            {
                _exBearingSendRequest = value;
                OnPropertyChanged();
            }
        }


        private ExBearing _exBearingSendAnswer = new ExBearing();

        public ExBearing ExBearingSendAnswer
        {
            get { return _exBearingSendAnswer; }
            set
            {
                _exBearingSendAnswer = value;
                OnPropertyChanged();
            }
        }


        private ExBearing _exBearingReceiveRequest = new ExBearing();

        public ExBearing ExBearingReceiveRequest
        {
            get { return _exBearingReceiveRequest; }
            set
            {
                _exBearingReceiveRequest = value;
                OnPropertyChanged();
            }
        }


        private ExBearing _exBearingReceiveAnswer = new ExBearing();

        public ExBearing ExBearingReceiveAnswer
        {
            get { return _exBearingReceiveAnswer; }
            set
            {
                _exBearingReceiveAnswer = value;
                OnPropertyChanged();
            }
        }



        private AutoExBearing _autoExecutiveBearing;

        public AutoExBearing AutoExecutiveBearing
        {
            get { return _autoExecutiveBearing; }
            set
            {
                _autoExecutiveBearing = value;
                OnPropertyChanged();
            }
        }



        private string _pathImageOrientation;

        public string PathImageOrientation
        {
            get { return _pathImageOrientation; }
            set
            {
                _pathImageOrientation = value;
                OnPropertyChanged();
            }
        }

        private float _angleOrientation = 0;

        public float AngleOrientation
        {
            get { return _angleOrientation; }
            set
            {
                _angleOrientation = value;
                OnPropertyChanged();
            }
        }



        

        public void SetDefaultDPanel()
        {
            sizeDPSetting.Visible = true;
            sizeDPTables.Visible = true;
            sizeDPJamming.Visible = false;
            sizeDPSpoofing.Visible = false;

            sizeDPSetting.Current = DefaultSize.WidthDPanelSetting;
            sizeDPTables.Current = DefaultSize.HeightDPanelJammer;
            sizeDPJamming.Current = 0;// DefaultSize.WidthDPanelJamming;
            sizeDPSpoofing.Current = 0;// DefaultSize.WidthDPanelSpoofing;

            

            sizeDPSetting.SetDefault();
            sizeDPTables.SetDefault();
            sizeDPJamming.SetDefault();
            sizeDPSpoofing.SetDefault();
        }

        public MainWindowViewModel()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");


            InitMarkSizeWnd();

            

            sizeDPSetting.PropertyChanged += SizeDP_PropertyChanged;
            sizeDPTables.PropertyChanged += SizeDP_PropertyChanged;
            sizeDPJamming.PropertyChanged += SizeDP_PropertyChanged;
            sizeDPSpoofing.PropertyChanged += SizeDP_PropertyChanged;

           

            GlobalPropertiesVM = new GrozaSModelsDBLib.GlobalProperties();
            LocalPropertiesVM = new LocalProperties();


            DirectionRX = new Direction();
            DirectionTX = new Direction();
            DirectionOEM = new Direction();
            DirectionBRD = new Direction();

         
            AutoExecutiveBearing = new AutoExBearing();

            InitZorkiRTargetVisible();

        }

        private void InitMarkSizeWnd()
        {
            markSizeWnd = SerializerJSON.Deserialize<MarkSizeWnd>(AppDomain.CurrentDomain.BaseDirectory + "SizePanel.json");

            if (markSizeWnd == null)
            {
                markSizeWnd = new MarkSizeWnd();
                sizeDPSetting = new SizeValue(DefaultSize.WidthDPanelSetting);
                sizeDPTables = new SizeValue(DefaultSize.HeightDPanelJammer);
                sizeDPJamming = new SizeValue(DefaultSize.WidthDPanelJamming);
                sizeDPSpoofing = new SizeValue(DefaultSize.WidthDPanelSpoofing);
                SetDefaultDPanel();
            }

            else
            {
                sizeDPSetting = markSizeWnd.sizeDPSetting;
                sizeDPTables = markSizeWnd.sizeDPTables;
                sizeDPJamming = markSizeWnd.sizeDPJamming;
                sizeDPSpoofing = markSizeWnd.sizeDPSpoofing;
            }
        }
        

        private void SizeDP_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

            markSizeWnd.sizeDPSetting = sizeDPSetting;
            markSizeWnd.sizeDPTables = sizeDPTables;
            markSizeWnd.sizeDPJamming = sizeDPJamming;
            markSizeWnd.sizeDPSpoofing = sizeDPSpoofing;

            SerializerJSON.Serialize<MarkSizeWnd>(markSizeWnd, AppDomain.CurrentDomain.BaseDirectory + "SizePanel.json");

        }

        private Dictionary<ViewZorkiR, bool> _zorkiRTargetVisible = new Dictionary<ViewZorkiR, bool>();
        public Dictionary<ViewZorkiR, bool> ZorkiRTargetVisible
        {
            get { return _zorkiRTargetVisible; }
            set
            {
                _zorkiRTargetVisible = value;
                OnPropertyChanged();
            }
        }

          
        public static IEnumerable<T> GetEnumValues<T>() where T: Enum
        { 
            return Enum.GetValues(typeof(T)).Cast<T>();
        }
        private void InitZorkiRTargetVisible()
        {
            foreach(var zrv in GetEnumValues<ViewZorkiR>())
                _zorkiRTargetVisible.Add(zrv, true);            
        }

        






        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));            
        }

        
    #endregion
}
}
