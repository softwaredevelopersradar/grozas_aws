﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Resources;
using System.Threading;
using System.Windows;
using ValuesCorrectLib;

namespace GrozaS_AWS
{
    

    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public  string GetResourceTitle (string key)
        {
            try
            {                
                return (string)this.Resources[key];
            }

            catch
            {
                return "";
            }
            
        }

        
        private void SetResourceLanguage(DllGrozaSProperties.Models.Languages language)
        {

            this.Resources.MergedDictionaries.Clear();
         
            ResourceDictionary dict = new ResourceDictionary();
            ResourceDictionary dictAddPanel = new ResourceDictionary();
            ResourceDictionary dictConnectionPanel = new ResourceDictionary();
            ResourceDictionary dictUISource = new ResourceDictionary();
            ResourceDictionary dictInformPanel = new ResourceDictionary();
            ResourceDictionary dictMapButtons = new ResourceDictionary();
            ResourceDictionary dictMapContextMenu = new ResourceDictionary();
            ResourceDictionary dictUIChat = new ResourceDictionary();

            try
            {
                switch (language)
                {
                    case DllGrozaSProperties.Models.Languages.EN:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.EN.xaml",
                                      UriKind.Relative);
                        dictAddPanel.Source = new Uri("/GrozaS_AWS;component/Languages/AddPanel/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictConnectionPanel.Source = new Uri("/GrozaS_AWS;component/Languages/ConnectionPanel/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictUISource.Source = new Uri("/GrozaS_AWS;component/Languages/UISource/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictInformPanel.Source = new Uri("/GrozaS_AWS;component/Languages/InformPanel/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictMapButtons.Source = new Uri("/GrozaS_AWS;component/Languages/MapButtons/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictMapContextMenu.Source = new Uri("/GrozaS_AWS;component/Languages/MapContextMenu/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictUIChat.Source = new Uri("/GrozaS_AWS;component/Languages/UIChat/StringResource.EN.xaml",
                                      UriKind.Relative);
                        break;

                    case DllGrozaSProperties.Models.Languages.RU:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.RU.xaml",
                                           UriKind.Relative);
                        dictAddPanel.Source = new Uri("/GrozaS_AWS;component/Languages/AddPanel/StringResource.RU.xaml",
                                           UriKind.Relative);
                        dictConnectionPanel.Source = new Uri("/GrozaS_AWS;component/Languages/ConnectionPanel/StringResource.RU.xaml",
                                      UriKind.Relative);
                        dictUISource.Source = new Uri("/GrozaS_AWS;component/Languages/UISource/StringResource.RU.xaml",
                                      UriKind.Relative);
                        dictInformPanel.Source = new Uri("/GrozaS_AWS;component/Languages/InformPanel/StringResource.RU.xaml",
                                      UriKind.Relative);
                        dictMapButtons.Source = new Uri("/GrozaS_AWS;component/Languages/MapButtons/StringResource.RU.xaml",
                                      UriKind.Relative);
                        dictMapContextMenu.Source = new Uri("/GrozaS_AWS;component/Languages/MapContextMenu/StringResource.RU.xaml",
                                      UriKind.Relative);
                        dictUIChat.Source = new Uri("/GrozaS_AWS;component/Languages/UIChat/StringResource.RU.xaml",
                                      UriKind.Relative);
                        break;

                    case DllGrozaSProperties.Models.Languages.AZ:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.AZ.xaml",
                                           UriKind.Relative);
                        dictAddPanel.Source = new Uri("/GrozaS_AWS;component/Languages/AddPanel/StringResource.AZ.xaml",
                                           UriKind.Relative);
                        dictConnectionPanel.Source = new Uri("/GrozaS_AWS;component/Languages/ConnectionPanel/StringResource.AZ.xaml",
                                      UriKind.Relative);
                        dictUISource.Source = new Uri("/GrozaS_AWS;component/Languages/UISource/StringResource.AZ.xaml",
                                      UriKind.Relative);
                        dictInformPanel.Source = new Uri("/GrozaS_AWS;component/Languages/InformPanel/StringResource.AZ.xaml",
                                      UriKind.Relative);
                        dictMapButtons.Source = new Uri("/GrozaS_AWS;component/Languages/MapButtons/StringResource.AZ.xaml",
                                      UriKind.Relative);
                        dictMapContextMenu.Source = new Uri("/GrozaS_AWS;component/Languages/MapContextMenu/StringResource.AZ.xaml",
                                      UriKind.Relative);
                        dictUIChat.Source = new Uri("/GrozaS_AWS;component/Languages/UIChat/StringResource.AZ.xaml",
                                      UriKind.Relative);
                        break;

                    default:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.EN.xaml",
                                      UriKind.Relative);
                        dictAddPanel.Source = new Uri("/GrozaS_AWS;component/Languages/AddPanel/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictConnectionPanel.Source = new Uri("/GrozaS_AWS;component/Languages/ConnectionPanel/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictUISource.Source = new Uri("/GrozaS_AWS;component/Languages/UISource/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictInformPanel.Source = new Uri("/GrozaS_AWS;component/Languages/InformPanel/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictMapButtons.Source = new Uri("/GrozaS_AWS;component/Languages/MapButtons/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictMapContextMenu.Source = new Uri("/GrozaS_AWS;component/Languages/MapContextMenu/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictUIChat.Source = new Uri("/GrozaS_AWS;component/Languages/UIChat/StringResource.EN.xaml",
                                      UriKind.Relative);
                        break;
                }

                

                this.Resources.MergedDictionaries.Add(dict);
                this.Resources.MergedDictionaries.Add(dictAddPanel);
                this.Resources.MergedDictionaries.Add(dictConnectionPanel);
                this.Resources.MergedDictionaries.Add(dictUISource);
                this.Resources.MergedDictionaries.Add(dictInformPanel);
                this.Resources.MergedDictionaries.Add(dictMapButtons);
                this.Resources.MergedDictionaries.Add(dictMapContextMenu);
                this.Resources.MergedDictionaries.Add(dictUIChat);
            }
            catch 
            { }
        }

        

        private void basicProperties_OnLanguageChanged(object sender, DllGrozaSProperties.Models.Languages language)
        {            
            SetResourceLanguage(language);
            TranslatorTables.LoadDictionary(language);

            RastrMap.SetResourceLanguage((MapLanguages)basicProperties.Local.General.Language);

            newWindow?.SetLanguage(basicProperties.Local.General.Language);
            SetViewMode();

        }


    }
}
