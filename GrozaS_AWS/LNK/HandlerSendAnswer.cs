﻿using GrozaSModelsDBLib;
using System;
using System.Linq;
using System.Windows;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        private bool SendResultSynchTime(int JammerReceiver)
        {
            if (matedGroup == null)
                return false;

            return matedGroup.SendResultTime(JammerReceiver, 0, DateTime.Now);
        }

        private bool SendResultCoord(int JammerReceiver)
        {
            if (matedGroup == null)
                return false;
            Coord coord = lJammerStation.Where(x => x.Role == StationRole.Own).FirstOrDefault().Coordinates;

            return matedGroup.SendConfirmCoord(JammerReceiver, 0, coord.Latitude, coord.Longitude);
        }

        private bool SendResultText(int JammerReceiver)
        {
            if (matedGroup == null)
                return false;

            return matedGroup.SendConfirmTextMessage(JammerReceiver, 0);
        }

        private bool SendResultState(int JammerReceiver)
        {
            if (matedGroup == null)
                return false;

            return matedGroup.SendConfirmState(JammerReceiver, 0);
        }

        private bool SendConfirmExecutiveBearing(int JammerReceiver)
        {
            if (matedGroup == null)
                return false;

            return matedGroup.SendConfirmExecutingBearing(JammerReceiver, 0);
        }

       
        private bool SendResultExecutiveBearing(Models.ExBearing exBearing, TypeQuery typeQuery)
        {
            if (matedGroup == null)
                return false;

            try
            {               
                return matedGroup.SendResultExecutingBearing(lJammerStation.Find(x => x.Role == StationRole.Linked).Id, 0,
                                                                                      exBearing.ID,
                                                                                      (int)exBearing.Frequency,
                                                                                      (short)exBearing.Bearing,
                                                                                      exBearing.Time, typeQuery);
            }
            catch
            {
                return false;
            }

        }
    }
}