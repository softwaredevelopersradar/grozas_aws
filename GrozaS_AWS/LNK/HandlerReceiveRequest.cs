﻿using GrozaSModelsDBLib;

using MatedJS.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using UserControl_Chat;
using WPFControlConnection;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        

        private void TextMessageLNK(object sender, TextEventArgs e)
        {
            try
            {

                SendResultText(e.Jammer);

                chatBuble.SetMessage(e.TextMessage);

                newWindow.DrawReceivedMessage(e.Jammer, e.TextMessage);
            }
            catch 
            {
                
            }
        }
     
        private void GetCoordLNK(object sender, CoordEventArgs e)
        {
            try
            {
                SendResultCoord(e.Jammer);

                UpdateCoordJammer(e.Jammer, e.Latitude, e.Longitude);
            }
            catch
            {

            }
        }

        private void GetTimeLNK(object sender, TimeEventArgs e)
        {
            try
            {
                SendResultSynchTime(e.Jammer);

                UpdateTimeSynchJammer(e.Jammer, e.Time);
            }
            catch
            { }
        }

        private void GetStateLNK(object sender, ErrorEventArgs e)
        {
            try
            {
                SendResultState(e.Jammer);
            }
            catch
            { }
        }

        private void GetExecutingBearingLNK(object sender, FBearingEventArgs e)
        {
            try
            {
                //send confirm
                SendConfirmExecutiveBearing(e.Jammer);


                // open window of detail executive bearing
                InitNotificationIAnswerWindow(mainWindowViewModel.AutoUpdateSource);

                mainWindowViewModel.ExBearingSendAnswer.Clear();


                try
                {
                    // show detail data 
                    var OwnJammer = lJammerStation.Where(x => x.Role == StationRole.Own).FirstOrDefault().Id;

                    mainWindowViewModel.ExBearingReceiveRequest = new Models.ExBearing() { StationNumber = (byte)e.Jammer, ID = e.ID, Frequency = e.Frequency, Bearing = e.Bearing, TimeAsk = DateTime.Now, Time = e.Time, Query = TypeQuery.ASK };



                    // if AutoMode
                    if (mainWindowViewModel.AutoUpdateSource)
                    {
                        mainWindowViewModel.ExBearingSendAnswer = new Models.ExBearing()
                        { 
                            StationNumber = (byte)OwnJammer,
                            Frequency = CurrentSourcesDF.Last().Track.Last().Frequency,
                            Bearing = CurrentSourcesDF.Last().Track.Last().Bearings.Where(x => x.Jammer == OwnJammer).FirstOrDefault().Bearing,
                            Time = CurrentSourcesDF.Last().Track.Last().Time
                        };

                       
                        SendResultExecutiveBearing(mainWindowViewModel.ExBearingSendAnswer, TypeQuery.ANSWERYES);


                        // if frequency is not worse than accuracy
                        if (Math.Abs(mainWindowViewModel.ExBearingReceiveRequest.Frequency - mainWindowViewModel.ExBearingSendAnswer.Frequency) 
                            <= mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.FrequencyAccuracy)
                        {
                            // Answer YES
                            StatusAutoAnswer(UINotificationWindow.StatusMessage.Accept);

                            // update coordinate
                            UpdateTrackSourceTDF(new Models.BearingItem()
                            {
                                Bearing = e.Bearing,
                                Jammer = lJammerStation.Where(x => x.Role == StationRole.Linked).FirstOrDefault().Id
                            });

                        }
                        else 

                            // Answer Another target
                            StatusAutoAnswer(UINotificationWindow.StatusMessage.Wrong);
                        return;
                    }
                

                


                // Hand Mode
                foreach (Models.SourceDF source in CurrentSourcesDF)
                {
                    if (Math.Abs(e.Frequency - source.Track.LastOrDefault().Frequency) <= mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.FrequencyAccuracy)
                    {                        
                        //idSource = source.ID;
                        mainWindowViewModel.ExBearingSendAnswer = new Models.ExBearing()
                        {
                            StationNumber = (byte)OwnJammer,
                            ID = source.ID,
                            Frequency = source.Track.LastOrDefault().Frequency,
                            Bearing = source.Track.LastOrDefault().Bearings.Where(x => x.Jammer == OwnJammer).LastOrDefault().Bearing,
                            TimeAsk = DateTime.Now,
                            Time = source.Track.LastOrDefault().Time,
                            Query = TypeQuery.ASK
                        };
                    }
                }

                }
                catch
                { }

            }
            catch
            {
               
            }
        }

       
    }
}