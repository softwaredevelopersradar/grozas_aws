﻿using GrozaSModelsDBLib;
using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Windows;
using WPFControlConnection;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {

        
        private void LNKControlConnection_ButServerClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (matedGroup != null)
                    DisconnectLNK();
                else
                    
                    ConnectLNK();
            }
            catch
            { }
        }

        private void OpenPortLNK(object sender, EventArgs e)
        {
            mainWindowViewModel.StateConnectionLNK = ConnectionStates.Connected;
        }

        private void ClosePortLNK(object sender, EventArgs e)
        {
            mainWindowViewModel.StateConnectionLNK = ConnectionStates.Disconnected;
        }


        private void UpdateCoordJammer(int IDJammer, double Latitude, double Longitude)
        {
            if (mainWindowViewModel.clientDB != null)
            {
                var jammer = lJammerStation.Where(x => x.Id == IDJammer).FirstOrDefault();
                jammer.Coordinates.Latitude = Latitude;
                jammer.Coordinates.Longitude = Longitude;

                mainWindowViewModel.clientDB.Tables[GrozaSModelsDBLib.NameTable.TableJammerStation].Change(jammer);
            }
        }


        private void UpdateTimeSynchJammer(int IDJammer, DateTime time)
        {

            if (mainWindowViewModel.clientDB != null)
            {
                TimeSpan timeSpan = DateTime.Now.Subtract(time);

                var jammer = lJammerStation.Find(x => x.Id == IDJammer);

                jammer.DeltaTime = timeSpan.ToString();

                mainWindowViewModel.clientDB.Tables[NameTable.TableJammerStation].Change(jammer);
            }

            
        }

    }
}

