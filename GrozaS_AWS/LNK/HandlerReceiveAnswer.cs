﻿using GrozaSModelsDBLib;

using MatedJS.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using UserControl_Chat;
using WPFControlConnection;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
      

        private void ConfirmTextMessageLNK(object sender, ErrorEventArgs e)
        {
            try
            {
                newWindow.ConfirmSentMessage(e.Jammer);
            }
            catch
            { }
        }

        private void ResultCoordLNK(object sender, CoordEventArgs e)
        {
            try
            {
                UpdateCoordJammer(e.Jammer, e.Latitude, e.Longitude);
            }
            catch
            { }
        }

        private void ResultTimeLNK(object sender, TimeEventArgs e)
        {
            try
            {
                UpdateTimeSynchJammer(e.Jammer, e.Time);
            }
            catch 
            { }
        }

        private void ConfirmStateLNK(object sender, ErrorEventArgs e)
        {
            try
            {
                
            }
            catch
            { }
        }
      
        private void ConfirmExecutingBearingLNK(object sender, ErrorEventArgs e)
        {
            try
            {
                ConfirmRequest();
            }
            catch
            { }
        }

        private void ResultExecutingBearingLNK(object sender, FBearingEventArgs e)
        {
            try
            {
                if (e.Error == 0)
                {




                    var linkedStation = lJammerStation.Find(x => x.Role == StationRole.Linked);
                    mainWindowViewModel.ExBearingReceiveAnswer = new Models.ExBearing() { StationNumber = (byte)linkedStation.Id, Frequency = e.Frequency, Bearing = e.Bearing, Time = e.Time, TimeAsk = DateTime.Now, ID = e.ID, Query = e.Type };




                    if (mainWindowViewModel.ExBearingReceiveAnswer.Query == TypeQuery.ANSWERNO)
                        DeclineAnswer();

                    if (mainWindowViewModel.ExBearingReceiveAnswer.Query == TypeQuery.ANSWERYES &&
                        (Math.Abs(mainWindowViewModel.ExBearingSendRequest.Frequency - mainWindowViewModel.ExBearingReceiveAnswer.Frequency) <= mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.FrequencyAccuracy))
                    {
                        AcceptAnswer();

                        UpdateTrackSourceTDF(mainWindowViewModel.ExBearingSendRequest.ID, new Models.BearingItem()
                        {
                            ID = e.ID,
                            Bearing = e.Bearing,
                            Jammer = lJammerStation.Where(x => x.Role == StationRole.Linked).FirstOrDefault().Id
                        });
                    }
                    else SuggestNewTarget();
               
                    RefreshDependenceSource();

                    CheckVoiceAlarm();

                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Error {0}", ex.Message));
            }
        }

        

    }
}