﻿
using ClientServerLib;
using System;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;




namespace GrozaS_AWS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        

        private void DefaultPanelToggleButton_Click(object sender, RoutedEventArgs e)
        {
            mainWindowViewModel.SetDefaultDPanel();
        }


        private void UpdateCmpRXPanelToggleButton_Click(object sender, RoutedEventArgs e)
        {
            GetCompassRX();
        }

        private void UpdateCmpTXToggleButton_Click(object sender, RoutedEventArgs e)
        {
            GetCompassTX();
        }

        private void SetTXToggleButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                mainWindowViewModel.DirectionBRD = mainWindowViewModel.SetAngleBRD(mainWindowViewModel.DirectionRX.Result, mainWindowViewModel.DirectionTX);
                
                UpdateMapDirectionBRD();

                comBRD.SendSetAngle(0, (short)mainWindowViewModel.DirectionBRD.Azimuth, 0);

            }
            catch { }

            

        }

        private void GetAgleBRDToggleButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                comBRD.SendGetAngle();
            }
            catch { }

        }

        private void StopBRDToggleButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                mainWindowViewModel.DirectionBRD = mainWindowViewModel.SetAngleBRD(mainWindowViewModel.DirectionTX.Result, mainWindowViewModel.DirectionTX);
                
                UpdateMapDirectionBRD();

                comBRD.SendStop(0);

            }
            catch { }
        }

        private void CalibrationToggleButton_Click(object sender, RoutedEventArgs e)
        {
            //MyEventArgsGetData coord = new MyEventArgsGetData(5, new object());

            //Random rnd = new Random();

            //clientGnss.reclocation.ReceiverLatitude = 51.920783 + rnd.Next(-2, 2);
            //clientGnss.reclocation.ReceiverLongitude = 30.928688 + rnd.Next(-2, 2);

            //HandlerReceiveData_ClientGnss(this, coord);

            try
            {
                comBRD.Calibration();

            }
            catch { }
            

        }

        private void TemplateUAVButton_Click(object sender, RoutedEventArgs e)
        {
            //SaveSourceTDF_DB();

            if (templateWindow.IsVisible)
                templateWindow.Hide();
            else
                templateWindow.Show();

            
        }


        private void IntelegenceModeButton_Click(object sender, RoutedEventArgs e)
        {
            SendMode(Mode.ELINT);

        }

        private void JammingModeButton_Click(object sender, RoutedEventArgs e)
        {
            SendMode(Mode.JAMMING);
        }

        private void StopModeButton_Click(object sender, RoutedEventArgs e)
        {
            SendMode(Mode.STOP);
        }

    }
}