﻿using AutoMapper;
using DLL_Compass;
using DllGrozaSProperties.Models;
using GrozaS_AWS.Controllers;
using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Media;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TableEvents;
using ValuesCorrectLib;
using WPFControlConnection;

namespace GrozaS_AWS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        MainWindowViewModel mainWindowViewModel;

        IMapper iMapper;

        System.Timers.Timer tmrAutoExecBearing = new System.Timers.Timer();

        System.Timers.Timer tmrAudioSignal = new System.Timers.Timer();

        SoundPlayer playAlarm;

        public MainWindow()
        {

            InitializeComponent();

            mainWindowViewModel = new MainWindowViewModel();


            mainWindowViewModel.DirectionRX.PropertyChanged += DirectionRX_PropertyChanged;
            mainWindowViewModel.DirectionTX.PropertyChanged += DirectionTX_PropertyChanged;
            mainWindowViewModel.DirectionOEM.PropertyChanged += DirectionOEM_PropertyChanged;

            mainWindowViewModel.AutoExecutiveBearing.PropertyChanged += AutoExecutiveBearing_PropertyChanged;
            mainWindowViewModel.PropertyChanged += MainWindowViewModel_PropertyChanged;


            CurrentSourcesDF.CollectionChanged += CurrentSourcesDF_CollectionChanged;



            this.DataContext = mainWindowViewModel;


            basicProperties.SetPassword("256");



            iMapper = MapperModels.InitializeAutoMapper().CreateMapper();

            InitLocalProperties();

            PropViewCoords.ViewCoords = (byte)mainWindowViewModel.LocalPropertiesVM.General.CoordinateView;

            ConnectCmpRX();

            ConnectCmpTX();

            InitTables();

            InitTemplateWnd();

            InitJammingControl();

            InitDroneControl();

            InitChat();

            ConnectOEM();
            ConnectTDF();

            ConnectClientDB();
            ConnectClientGnss();


            SetResourceLanguage(basicProperties.Local.General.Language);
            TranslatorTables.LoadDictionary(basicProperties.Local.General.Language);

            RastrMap.SetResourceLanguage((MapLanguages)basicProperties.Local.General.Language);
            newWindow.SetLanguage(basicProperties.Local.General.Language);
            InitializeTimerAutoExecBearing();

            InitializeTimerAudioSignal();

            

        }

        private void MainWindowViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "TempMode":
                    SetViewMode();
                    break;

                default:
                    break;
            }
        }

        private void SetViewMode()
        {
            mainWindowViewModel.ViewMode = GetResourceTitle((string)Enum.GetName(typeof(Mode), mainWindowViewModel.TempMode));
        }

        private void InitializeTimerAutoExecBearing()
        {
            tmrAutoExecBearing.Elapsed += Timer_Elapsed;
            tmrAutoExecBearing.Interval = 3000;
            tmrAutoExecBearing.AutoReset = true;
        }

        private void InitializeTimerAudioSignal()
        {
            tmrAudioSignal.Elapsed += TickTimerAudioSignal;
            
            tmrAudioSignal.AutoReset = true;
        }

        private void CurrentSourcesDF_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
                RefreshDependenceSource();

            CheckVoiceAlarm();


        }


        private void CheckVoiceAlarm()
        {
            if (CurrentSourcesDF != null && CurrentSourcesDF.Count > 0)
            {
                var _countAlarm = 0;
                foreach (var src in CurrentSourcesDF)
                {
                    if (src.Track!= null && src.Track.Count>0 && src.Track.Last().Zone == Zones.ALARM)
                        _countAlarm++;
                }
                    

                if (_countAlarm > 0)
                    StartAudioSignal();
                else
                    StopAudioSignal();
            }

            
            
                
        }

        private void StartAudioSignal()
        {
            tmrAudioSignal.Interval = 1000;
            tmrAudioSignal.Enabled = true;
           
        }

        private void StopAudioSignal()
        {
            tmrAudioSignal.Enabled = false;

            try
            {
                playAlarm.Stop();
            }
            catch { }
            
        }

        private void RefreshDependenceSource()
        {

            try
            {
                List<TableSource> tableSourceConvert = new List<TableSource>();

                foreach (var src in CurrentSourcesDF)
                {
                    ObservableCollection<TableTrack> trackConvert = new ObservableCollection<TableTrack>();

                    foreach (var trc in src.Track)
                    {
                        ObservableCollection<TableJamBearing> bearingConvert = new ObservableCollection<TableJamBearing>();

                        foreach (var brn in trc.Bearings)
                        {
                            bearingConvert.Add(new TableJamBearing()
                            {
                                Id = brn.ID,
                                Bearing = brn.Bearing,
                                Distance = brn.Distance,
                                NumJammer = brn.Jammer
                            });
                        }



                        trackConvert.Add(new TableTrack()
                        {
                            Id = trc.ID,
                            Time = trc.Time,
                            FrequencyMHz = trc.Frequency,
                            FrequencyRX = trc.FrequencyRX,
                            BandMHz = trc.Band,
                            Coordinates = trc.Coordinate,
                            Bearing = bearingConvert

                        });
                    }

                    tableSourceConvert.Add(new TableSource()
                    {
                        Id = src.ID,
                        Type = src.Type,    
                        TypeRSM = (byte)src.TypeRSM,
                        Note = src.Note,
                        Track = trackConvert
                    });

                };

                

                DroneControl_UpdateTableSource(tableSourceConvert);
                UpdateMapBearing(lJammerStation, CurrentSourcesDF);
                UpdateSourceMap(CurrentSourcesDF);
            }
            catch
            { }
        }

      

        private void AutoExecutiveBearing_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (mainWindowViewModel.AutoExecutiveBearing.AutoExecBearing)
                tmrAutoExecBearing.Start();
            else
                tmrAutoExecBearing.Stop();
        }

        

        public void LocalPropertiesVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

            mainWindowViewModel.DirectionRX.CourseAngle = mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle;
            mainWindowViewModel.DirectionRX.Error = mainWindowViewModel.LocalPropertiesVM.CmpRX.ErrorDeg;

            //mainWindowViewModel.DirectionTX.CourseAngle = (mainWindowViewModel.GlobalPropertiesVM.CmpTX.Angle > mainWindowViewModel.DirectionTX.Azimuth) ?
            //    (mainWindowViewModel.GlobalPropertiesVM.CmpTX.Angle - mainWindowViewModel.DirectionTX.Azimuth) : (360 - (mainWindowViewModel.DirectionTX.Azimuth - mainWindowViewModel.GlobalPropertiesVM.CmpTX.Angle));

            UpdateCourseAngleCmpTX();

            //mainWindowViewModel.DirectionTX.Error = mainWindowViewModel.LocalPropertiesVM.CmpTX.ErrorDeg;

            mainWindowViewModel.DirectionOEM.CourseAngle = mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle;
            mainWindowViewModel.DirectionOEM.Error = mainWindowViewModel.LocalPropertiesVM.EOM.ErrorDeg;

        }

       

        private void Map_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
         
            RefreshDependenceSource();

            HandlerLocalProperties(this, mainWindowViewModel.LocalPropertiesVM);
        }

        private void DirectionOEM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            UpdateMapDirectionOEM(10);
        }

        private void DirectionTX_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            UpdateMapDirectionTX();
        }

        private void DirectionRX_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            UpdateMapDirectionRX();
        }



        private void Window_Closing(object sender, CancelEventArgs e)
        {

            SaveSourceTDF_DB();

            try 
            {
                if (tcpServerPC != null)
                    tcpServerPC.DestroyServer();
            }
            catch { }


            try
            {
                if (comBRD != null)
                    comBRD.SendSetAngle(0, (short)mainWindowViewModel.LocalPropertiesVM.BRD.TransportAngle, 0);
            }
            catch { }


            System.Windows.Threading.Dispatcher.ExitAllFrames();
        }


        

        

        
        void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                SendGetExecutiveBearing(CurrentSourcesDF.Last().ID, TypeQuery.ASK);
                ShowSentExecutingBearingRequest(CurrentSourcesDF.Last(), TypeQuery.ASK);
            }
            
            catch { }
        }

        void TickTimerAudioSignal(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                playAlarm.Play();
            }
            catch { }

        }

        

        private void ChangeLanguage(DllGrozaSProperties.Models.Languages newLanguage)
        {
            SetDynamicResources(newLanguage);
            //LoadDictionary();
            //SetCategoryGlobalNames();
            //SetCategoryLocalNames();
        }


        private void SetDynamicResources(DllGrozaSProperties.Models.Languages newLanguage)
        {
            try
            {
                ResourceDictionary dict = new ResourceDictionary();
                switch (newLanguage)
                {
                    case DllGrozaSProperties.Models.Languages.EN:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/AddPanel/StringResource.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case DllGrozaSProperties.Models.Languages.RU:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/AddPanel/StringResource.RU.xaml",
                                           UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/AddPanel/StringResource.EN.xaml",
                                          UriKind.Relative);
                        break;
                }
                Resources.MergedDictionaries.Add(dict);
            }
            catch 
            {
                
            }

        }

        
    }
}
