﻿using System;
using System.Collections.Generic;
using GrozaSModelsDBLib;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using UISource;
using System.IO;
using System.Linq;

namespace GrozaS_AWS
{
    public partial class MainWindow
    {
        Dictionary<byte, string> TypeCodeCollection = new Dictionary<byte, string>() { { 255, "No Image" }  };

        int SelectedID = -1;
        public void InitDroneControl()
        {
            DroneControl_UpdateSourcesCollection();

            ItemDrone.OnAddRecord += ItemDrone_OnAddRecord;
            ItemDrone.OnChangeRecord += ItemDrone_OnChangeRecord;
            ItemDrone.OnDeleteRecord += ItemDrone_OnDeleteRecord;
            ItemDrone.OnClearRecords += ItemDrone_OnClearRecords;
            ItemDrone.OnDeleteTrackRecord += ItemDrone_OnDeleteTrackRecord;
            ItemDrone.OnAskBearing += ItemDrone_OnAskBearing;
            ItemDrone.OnSendToJamming += ItemDrone_OnSendToJamming; ;
            ItemDrone.OnSendToRS += ItemDrone_OnSendToRS;
            ItemDrone.OnDoubleClickDrone += ItemDrone_OnDoubleClickDrone;
            ItemDrone.OnSelectionChanged += ItemDrone_OnSelectionChanged;
            ItemDrone.OnSaveSource += ItemDrone_OnSaveSource;
            ItemDrone.OnIsWindowPropertyOpen += ItemDrone_OnIsWindowPropertyOpen;
        }

        

        private void ItemDrone_OnIsWindowPropertyOpen(object sender, DronePropertyView e)
        {
            e.SetLanguagePropertyGrid(basicProperties.Local.General.Language);
        }


        private void ItemDrone_OnSelectionChanged(object sender, RecDroneModel e)
        {

            SelectedID = e.ID;
            UpdateMapBearing(lJammerStation, CurrentSourcesDF);
        }


        private void ItemDrone_OnDoubleClickDrone(object sender, RecDroneModel e)
        {           
            Coord coordCenter = CurrentSourcesDF.Where(x => x.ID == e.ID).FirstOrDefault().Track.Last().Coordinate;
            mainWindowViewModel.LocationCenter = coordCenter;
        }


        private void DroneControl_UpdateSourcesCollection()
        {
            ItemDrone.DroneImageCollection = CreateImageDictionary();
            DroneControl_InitDroneTypeDictionary(TypeCodeCollection);
            JammingDrone_InitDroneTypeDictionary(TypeCodeCollection);
        }


        public void DroneControl_InitDroneTypeDictionary(Dictionary<byte, string> types)
        {
            ItemDrone.InitTypes(types);
        }


        private Dictionary<string, ImageSource> CreateImageDictionary()
        {
            var tempCollection = new Dictionary<string, ImageSource>();

            foreach (KeyValuePair<byte, string> code in TypeCodeCollection.Where(x => x.Key != 255).ToList())
            { TypeCodeCollection.Remove(code.Key); }

            foreach (TablePattern pattern in lTablePattern)
            {
                byte code = (byte)pattern.Id;

                if (!TypeCodeCollection.ContainsKey(code))
                {
                    tempCollection.Add(pattern.Name, pattern.Image);
                    TypeCodeCollection.Add(code, pattern.Name);
                }
            }

            return tempCollection;
        }


        //private Dictionary<string, ImageSource> CreateImageDictionary()
        //{
        //    var tempCollection = new Dictionary<string, ImageSource>();

        //    if (Directory.Exists(mainWindowViewModel.LocalPropertiesVM.General.FolderImagesDrones))
        //    {
        //        foreach (KeyValuePair<byte, string> code in TypeCodeCollection.Where(x => x.Key != 255).ToList())
        //        { TypeCodeCollection.Remove(code.Key); }

        //        var files = Directory.EnumerateFiles(mainWindowViewModel.LocalPropertiesVM.General.FolderImagesDrones, "*.*", SearchOption.AllDirectories)
        //                .Where(s => s.EndsWith(".png", StringComparison.OrdinalIgnoreCase) || s.EndsWith(".jpg", StringComparison.OrdinalIgnoreCase)).ToList();

        //        foreach (string file in files)
        //        {
        //            var fileName = Path.GetFileNameWithoutExtension(file);
        //            int tempPos = fileName.IndexOf('_');
        //            if (tempPos > 0 && CheckNums(fileName, tempPos))
        //            {
        //                var bitmap = new BitmapImage();
        //                bitmap.BeginInit();
        //                bitmap.UriSource = new Uri(file, UriKind.Absolute);
        //                bitmap.EndInit();

        //                byte code = byte.Parse(Convert.ToString(fileName.Substring(0, tempPos)));

        //                if (!TypeCodeCollection.ContainsKey(code))
        //                {
        //                    tempCollection.Add(fileName.Remove(0, tempPos + 1), bitmap);
        //                    TypeCodeCollection.Add(code, fileName.Remove(0, tempPos + 1));
        //                }
        //            }
        //        }
        //    }

        //    return tempCollection;
        //}


        private bool CheckNums(string fileName, int pos)
        {
            bool result = true;

            for (int i = 0; i < pos; i++)
            {
                result = result && Char.IsNumber(fileName[i]);
            }
            return result;    
        }
    }
}
