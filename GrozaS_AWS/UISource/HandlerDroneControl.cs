﻿using System;
using GrozaSModelsDBLib;
using UISource;
using System.Threading;
using System.Linq;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using Bearing;
using GrozaS_AWS.Models;
using System.IO;
using RecognitionSystemProtocol;
using GrozaS_AWS.RS;

namespace GrozaS_AWS
{
    public partial class MainWindow
    {
        #region DataBase EventHandlers

        private async void DroneControl_LoadDrones()
        {
            try
            {
                var tablSource = await mainWindowViewModel.clientDB.Tables[NameTable.TableSource].LoadAsync<TableSource>();
                DroneControl_UpdateTableSource(tablSource);
            }
            catch { }
        }


        private void DroneControl_UpdateTableSource(List<TableSource> e)
        {
            var ownStation = lJammerStation.Find(x => x.Role == StationRole.Own);
            var anotherStation = lJammerStation.Find(x => x.Role == StationRole.Linked);
            var DroneList = new ObservableCollection<RecDroneModel>();

            if (ownStation == null)
                return;

            foreach (TableSource rec in e)
            {
                var drone = new RecDroneModel()
                {
                    ID = rec.Id,
                    TypeRSM = (TypeBelong)((byte)rec.TypeRSM),
                    Band = rec.Track.Last().BandMHz,
                    Frequency = rec.Track.Last().FrequencyMHz,
                    FrequencyRX = rec.Track.Last().FrequencyRX,
                    TimeUpdate = rec.Track.Last().Time,
                    Latitude = rec.Track.Last().Coordinates.Latitude,
                    Longitude = rec.Track.Last().Coordinates.Longitude,
                    DistanceOwn = rec.Track.Last().Bearing.Where(t => t.NumJammer == ownStation.Id).FirstOrDefault().Distance,
                    BearingOwn = rec.Track.Last().Bearing.Where(t => t.NumJammer == ownStation.Id).FirstOrDefault().Bearing,
                };

                var tempAnotherBearing = anotherStation == null ? null : rec.Track.Last().Bearing.Where(t => t.NumJammer == anotherStation.Id).FirstOrDefault();

                drone.DistanceAnother = tempAnotherBearing == null ? 0 : tempAnotherBearing.Distance;
                drone.BearingAnother = tempAnotherBearing == null ? -1 : tempAnotherBearing.Bearing;

                if (TypeCodeCollection.ContainsKey(rec.Type))
                {
                    drone.Type = TypeCodeCollection[rec.Type];
                }
                else
                {
                    drone.Type = "No Image";
                }

                DroneList.Add(drone);
            }

            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
              ItemDrone.Drones = DroneList;
            });
        }
        #endregion



        #region Control EventHandlers

        private void ItemDrone_OnAddRecord(object sender, DronePropertiesModel e)
        {
            try
            {
                var ownStation = lJammerStation.Find(x => x.Role == StationRole.Own).Id;

                var anotherStation = 0;
                try
                {
                    anotherStation = lJammerStation.Find(x => x.Role == StationRole.Linked).Id;
                }
                catch
                { }


                ObservableCollection<TableJamBearing> bear = new ObservableCollection<TableJamBearing>();

                bear.Add(new TableJamBearing()
                {
                    NumJammer = ownStation,
                    Bearing = e.BearingOwn,
                    Distance = e.Length
                });

                if (anotherStation > 0)
                    bear.Add(new TableJamBearing()
                    {
                        NumJammer = anotherStation,
                        Bearing = e.BearingAnother,
                        Distance = 0
                    });

                Random rnd = new Random();

                var record = new TableSource()
                {
                    Type = e.Type,
                    //TypeRSM = (byte)(rnd.Next(0,5)),
                    Track = new ObservableCollection<TableTrack>()
                {
                    new TableTrack()
                    {

                        Time = DateTime.Now,
                        BandMHz = e.Band,
                        FrequencyMHz = e.Frequency,
                        FrequencyRX = e.Frequency-5,
                        Bearing = new ObservableCollection<TableJamBearing>(bear)
                       
                    }
                }
                };


                AddSourceTDF_DB(record);
            }
            catch
            { }
            
        }


        private  void ItemDrone_OnChangeRecord(object sender, DronePropertiesModel e)
        {

            var ownStation = lJammerStation.Find(x => x.Role == StationRole.Own).Id;

            var anotherStation = 0;
            try
            {
                anotherStation = lJammerStation.Find(x => x.Role == StationRole.Linked).Id;
            }
            catch
            { }


            ObservableCollection<BearingItem> bear = new ObservableCollection<BearingItem>();

            bear.Add(new BearingItem()
            {
                Jammer = ownStation,
                Bearing = e.BearingOwn,
                Distance = e.Length
            });

            if (anotherStation > 0)
                bear.Add(new BearingItem()
                {
                    Jammer = anotherStation,
                    Bearing = e.BearingAnother,
                    Distance = 0
                });




            var sourceEdit = CurrentSourcesDF.Where(x=>x.ID == e.Id).FirstOrDefault();
            sourceEdit.Type = e.Type;
            sourceEdit.AddTrackPoint(new TrackPoint()
            {

                Frequency = e.Frequency,
                FrequencyRX = e.Frequency,
                Band = e.Band,
                Time = DateTime.Now,

                Bearings = new ObservableCollection<BearingItem>(bear),
                Jammers = new ObservableCollection<TableJammerStation>(lJammerStation)


            }) ;

            
            CurrentSourcesDF.Where(x => x.ID == e.Id).FirstOrDefault().Type = e.Type;
            //CurrentSourcesDF.Where(x => x.ID == e.Id).FirstOrDefault().TypeRSM = TypeRSMart.EMPTY;
            CurrentSourcesDF.Where(x => x.ID == e.Id).FirstOrDefault().AddTrackPoint(new TrackPoint()
            {

                Frequency = e.Frequency,
                Band = e.Band,
                Time = DateTime.Now,
                Bearings = new ObservableCollection<BearingItem>(bear),
                Jammers = new ObservableCollection<TableJammerStation>(lJammerStation)


            });

            lastSource = null;

            SaveSourceTDF_DB();

        }


        private void ItemDrone_OnDeleteRecord(object sender, DronePropertiesModel e)
        {
          
            DeleteSourceTDF_DB(e.Id);
        }


        private void ItemDrone_OnClearRecords(object sender, EventArgs e)
        {
            ClearSourceTDF_DB();
        }


        private async void ItemDrone_OnDeleteTrackRecord(object sender, DronePropertiesModel e)
        {
            
            TrackPoint track = CurrentSourcesDF.Where(x => x.ID == e.Id).FirstOrDefault().Track.Last();

            CurrentSourcesDF.Where(x => x.ID == e.Id).FirstOrDefault().Track.Clear();
            CurrentSourcesDF.Where(x => x.ID == e.Id).FirstOrDefault().Track.Add(track);

            lastSource = null;

            SaveSourceTDF_DB();

        }


        private void ItemDrone_OnAskBearing(object sender, RecDroneModel e)
        {

            ShowSentExecutingBearingRequest(e, TypeQuery.ASK);
            SendGetExecutiveBearing(e.ID, TypeQuery.ASK);
            
        }



        private void ItemDrone_OnSendToJamming(object sender, DronePropertiesModel e)
        {
            if (mainWindowViewModel.clientDB == null)
                return;

            var drone = CurrentSourcesDF.FirstOrDefault(c => c.ID == e.Id);

            var jamRecord = new TableSuppressSource()
            {
                InputType = TypeInput.Auto,
                TableSourceId = drone.ID,
                FrequencyMHz = drone.Track.Last().Frequency,
                BandMHz = drone.Track.Last().Band,
                Type = drone.Type,
                LevelOwn = 0// (short)drone.Track.Last().Bearing.Last().Level
            };

            if (e.Frequency >= 100 && e.Frequency < 500)
                jamRecord.Id = 1;
            else if (e.Frequency >= 500 && e.Frequency < 2500)
                jamRecord.Id = 2;
            else if (e.Frequency >= 2500 && e.Frequency <= 6000)
                jamRecord.Id = 3;

            mainWindowViewModel.clientDB?.Tables[NameTable.TableSuppressSource].ChangeAsync(jamRecord);
        }


        private async void ItemDrone_OnSendToRS(object sender, DronePropertiesModel e)
        {
            
            try
            {
                if (clientRS != null)
                {
                    SetFrequencyRequest sourceRequest = new SetFrequencyRequest()
                    {
                        ID = (short)e.Id,
                        FrequencyMHz = e.Frequency,
                        ReceiverFrequencyMHz = e.FrequencyRX,
                        BandwidthMHz = e.Band,

                    };

                    var res = await SendRequestIFF(sourceRequest);

                    CurrentSourcesDF.Where(x => x.ID == e.Id).FirstOrDefault().TypeRSM = (TypeRSMart)Convert.ToByte(res.Type);
                }
            }
            catch
            { }
            
        }

           
        private void ItemDrone_OnSaveSource(object sender, ObservableCollection<RecDroneModel> e)
        {
            List<string> list = new List<string>();
            foreach (RecDroneModel source in e)
            {
                list.Add("ID: " + source.ID.ToString());
                list.Add("Type: " + source.Type.ToString());
                list.Add("TypeRSM: " + source.TypeRSM.ToString());
                list.Add("Frequency: " + source.Frequency.ToString() + " MHz");
                list.Add("FrequencyRX: " + source.FrequencyRX.ToString() + " MHz");
                list.Add("Band: " + source.Band.ToString() + " MHz");
                list.Add("Update time: " + source.TimeUpdate.ToString());
                list.Add("Bearing Own: " + source.BearingOwn.ToString() + "°");
                list.Add("Distance Own: " + (source.DistanceOwn / 1000).ToString("F3") + " km");
                list.Add("Bearing Linked: " + source.BearingAnother.ToString() + "°");
                list.Add("Distance Linked: " + (source.DistanceAnother / 1000).ToString("F3") + " km");
                list.Add("Latitude: " + source.Latitude.ToString());
                list.Add("Longitude: " + source.Longitude.ToString() + "\n");
                list.Add( "\n");
            }

            string[,] strFull = new string[list.Count, 1];
            for (int i = 0; i < list.Count; i++)
            {
                strFull[i, 0] = list[i];
            }

            if (Directory.Exists(mainWindowViewModel.LocalPropertiesVM.General.FolderRI_UAV))
            {
                string actualDirectory = mainWindowViewModel.LocalPropertiesVM.General.FolderRI_UAV + "\\" + DateTime.Now.ToString("dd-MMM-yy");

                if (!Directory.Exists(actualDirectory))
                {
                    Directory.CreateDirectory(actualDirectory);
                }

                RepToFile.RepToFile.SaveUpdateToTxt(actualDirectory, "\\" + DateTime.Now.ToString("HH-mm-ss") + ".txt", strFull);
            }
        }
        #endregion



    }
}
