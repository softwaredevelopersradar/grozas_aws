﻿
using RecognitionSystemProtocol;
using System.Windows;


namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        RecognitionSystemClient clientRS;

        private async void ConnectRS()
        {
            try
            {
                clientRS = new RecognitionSystemClient();
                clientRS.OnConnected += ClientRS_IsConnected;

           

                await clientRS.IQConnectToServer(mainWindowViewModel.LocalPropertiesVM.RS.IpAddress, mainWindowViewModel.LocalPropertiesVM.RS.Port);

                UpdateListOwnUAV();
            }
            
            catch { }
        }

        

        private void DisconnectRS()
        {
            try
            {
                clientRS.DisconnectFromServer();

                clientRS.OnConnected -= ClientRS_IsConnected;
                
                clientRS = null;
            }
            
            catch{ }
        }

    }
}
