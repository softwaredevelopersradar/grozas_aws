﻿using GrozaS_AWS.RS;
using GrozaSModelsDBLib;
using RecognitionSystemProtocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TableEvents;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        
        private void RSConnection_Click(object sender, RoutedEventArgs e)
        {
            if (clientRS == null)
                ConnectRS();
            else
                DisconnectRS();
        }

        
        private void ClientRS_IsConnected(bool isConnected)
        {
            if (isConnected)
            {
                mainWindowViewModel.StateConnectionRS = WPFControlConnection.ConnectionStates.Connected;

                UpdateListOwnUAV();
            }
            else
                mainWindowViewModel.StateConnectionRS = WPFControlConnection.ConnectionStates.Disconnected;

        }

        
        private async Task<AffiliationUAVResponse> SendRequestIFF(SetFrequencyRequest sourceRequest)
        {
            
            try
            {
                
                var answer = await clientRS.AffiliationUAV(sourceRequest.ReceiverFrequencyMHz, sourceRequest.FrequencyMHz, sourceRequest.BandwidthMHz);

                return answer;
            }
            catch 
            {
                return null;
            }
           
        }

        private async Task<SetActiveListFriendlyUAVsResponse> SendSetListOWnUAV(IsActiveUAV[] listUAV)
        {
            try
            {
                var answer = await clientRS.SetActiveListFriendlyUAVs(listUAV);

                return answer;

            }
            catch
            {
                return null;
            }

        }

        private async Task<byte> SendAddOWnUAV(UAV uav)
        {
            try
            {
                var answer = await clientRS.AddFriendlyUAV(uav);

                return (answer.Header.ErrorCode);

            }
            catch
            {
                return 255;
            }

        }

        private async Task<byte> SendDeleteOwnUAV(UAV uav)
        {
            try
            {
                var answer = await clientRS.DeleteFriendlyUAV(uav.ID);

                return (answer.Header.ErrorCode);

            }
            catch
            {
                return 255;
            }

        }

        private async Task<byte> SendClearOWnUAV()
        {
            try
            {
                var answer = await clientRS.DeleteFriendlyUAVs();

                return (answer.Header.ErrorCode);

            }
            catch
            {
                return 255;
            }

        }




        private async void UpdateListOwnUAV()
        {
            if (lOwnUAV == null || lOwnUAV.Count == 0 || clientRS == null || clientRS.IsConnected == false)
                return;
            try
            {

                IsActiveUAV[] listUAV = null;

                int index = 0;
                foreach (var own in lOwnUAV)
                {
                   
                    foreach (var item in own.Frequencies)
                    {
                        if (listUAV == null)
                            listUAV = new IsActiveUAV[1];
                        else

                            Array.Resize(ref listUAV, listUAV.Length + 1);
                        listUAV[listUAV.Length - 1] = new IsActiveUAV()
                        {
                            ActiveUAV = new UAV()
                            {
                                ID = Encoding.ASCII.GetBytes(own.SerialNumber),
                                FreqMinMHz = item.FrequencyMHz,
                                BandwidthMHz = item.BandMHz

                            },
                            isActive = Convert.ToByte(item.IsActive)
                        };
                        index++;
                    }
                }


                
                var listUAV_RS = await SendSetListOWnUAV(listUAV);


                UpdateStateUAV_RS(listUAV_RS);

            }
            catch
            { }
        }

        private void UpdateStateUAV_RS(SetActiveListFriendlyUAVsResponse res)
        {
         
            for (int i=0; i<res.IsFileSignalUAVs.Length; i++)
            {
                string str = Encoding.UTF8.GetString(res.IsFileSignalUAVs[i].FileSignalUAV.ID, 0, res.IsFileSignalUAVs[i].FileSignalUAV.ID.Length);
                TableOwnUAV u = lOwnUAV.Where(x => x.SerialNumber == str).FirstOrDefault();

                u.Frequencies.Where(x => x.FrequencyMHz == res.IsFileSignalUAVs[i].FileSignalUAV.FreqMinMHz).FirstOrDefault().
                    SR = Convert.ToBoolean(res.IsFileSignalUAVs[i].isFileSignal);

                flagUpdateSR = true;
                OnChangeRecord(this, new TableEvent(u));
            }

            //ChangeRangeOwnUAV(lLastOwnUAV);

        }

        private void ChangeRangeOwnUAV(List<TableOwnUAV> ownUAV)
        {
            try
            {
                if (mainWindowViewModel.clientDB == null)
                    return;
                
                foreach (var it in ownUAV)
                    OnChangeRecord(this, new TableEvent(it));

            }
            catch
            { }
        }

        private async void DeleteOwnUAV(TableEvent e)
        {
            if (e.NameTable == NameTable.TableOwnUAV)
            {
                TableOwnUAV tableOwnUAV = e.Record as TableOwnUAV;

                UAV uav = new UAV();
                uav.ID = Encoding.ASCII.GetBytes(tableOwnUAV.SerialNumber);
               
                await SendDeleteOwnUAV(uav);
            }
        }

        private async void ClearOwnUAV(NameTable e)
        {
            if (e == NameTable.TableOwnUAV)
            {
                var answer = await SendClearOWnUAV();

            }
        }

    }
} 