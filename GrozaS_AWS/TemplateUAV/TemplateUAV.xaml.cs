﻿using DBSourceCtrl.Models;
using GrozaSModelsDBLib;
using ItemFreqCtrl.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GrozaS_AWS.TemplateUAV
{
    /// <summary>
    /// Interaction logic for TemplateUAV.xaml
    /// </summary>
    public partial class TemplateUAV : Window
    {
        public event EventHandler<PropertyTemplate> OnAddTemplateUAV;
        public event EventHandler<PropertyTemplate> OnChangeTemplateUAV;
        public event EventHandler<PropertyTemplate> OnDeleteTemplateUAV;
        public event EventHandler OnClearTemplateUAV;
        public event EventHandler OnLoadDefaultTemplateUAV;
        

        public TemplateUAV()
        {
            InitializeComponent();

            
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
         
        }

        
        private void listTemplateUAV_OnAddRecord(object sender, PropertyTemplate e)
        {
            OnAddTemplateUAV?.Invoke(this, e);
        }

        private void listTemplateUAV_OnChangeRecord(object sender, PropertyTemplate e)
        {
            OnChangeTemplateUAV?.Invoke(this, e);
        }

        private void listTemplateUAV_OnDeleteRecord(object sender, PropertyTemplate e)
        {
            OnDeleteTemplateUAV?.Invoke(this, e);
        }

        private void listTemplateUAV_OnClearRecords(object sender, EventArgs e)
        {
            OnClearTemplateUAV?.Invoke(this, e);
        }

        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void listTemplateUAV_OnLoadDefaultRecords(object sender, EventArgs e)
        {
            OnLoadDefaultTemplateUAV?.Invoke(this, e);
        }
    }
}
