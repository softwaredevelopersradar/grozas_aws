﻿
using DBSourceCtrl.Models;
using GrozaSModelsDBLib;
using ItemFreqCtrl.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        private GrozaS_AWS.TemplateUAV.TemplateUAV templateWindow;


        private void InitTemplateWnd()
        {
            templateWindow = new TemplateUAV.TemplateUAV();

            templateWindow.OnAddTemplateUAV += TemplateWindow_OnAddTemplateUAV;
            templateWindow.OnChangeTemplateUAV += TemplateWindow_OnChangeTemplateUAV;
            templateWindow.OnClearTemplateUAV += TemplateWindow_OnClearTemplateUAV;
            templateWindow.OnDeleteTemplateUAV += TemplateWindow_OnDeleteTemplateUAV;
            templateWindow.OnLoadDefaultTemplateUAV += TemplateWindow_OnLoadDefaultTemplateUAV;

        }

        private void TemplateWindow_OnLoadDefaultTemplateUAV(object sender, EventArgs e)
        {
            DefaultTemplates();
        }

        private void UpdateTablePattern()
        {
    
            try
            {

                Dispatcher.BeginInvoke(new Action(() =>
            {
                ObservableCollection<TemplateSource> templateSource = new ObservableCollection<TemplateSource>();

                foreach (var t in lTablePattern)
                {
                    ObservableCollection<FreqWork> freqWork = new ObservableCollection<FreqWork>();
                    foreach (var f in t.FrequencyList)
                    {
                        freqWork.Add(new FreqWork
                        {
                            FrequencyMin = f.FrequencyMin,
                            FrequencyMax = f.FrequencyMax,
                            Band = f.Band
                        });
                    };


                    templateSource.Add(new TemplateSource
                    {
                        ID = t.Id,
                        Name = t.Name,
                        FreqWork = new ObservableCollection<FreqWork>(freqWork),
                        View = (DBSourceCtrl.ViewUAV)t.View,
                        Image = t.Image
                    });
                }

                templateWindow.listTemplateUAV.TemplateSources = templateSource;
            }), DispatcherPriority.Background);
            }

            catch { }


        }

        private void TemplateWindow_OnDeleteTemplateUAV(object sender, DBSourceCtrl.Models.PropertyTemplate e)
        {
            if (e != null)
            {
                TablePattern tablePattern = new TablePattern();
                
                tablePattern.Id = e.Id;

                DeletePatternRecord(tablePattern);

            }
        }

        private void TemplateWindow_OnClearTemplateUAV(object sender, EventArgs e)
        {
            ClearPatternRecord();
        }

        private void TemplateWindow_OnChangeTemplateUAV(object sender, DBSourceCtrl.Models.PropertyTemplate e)
        {
            if (e != null)
            {
                TablePattern tablePattern = new TablePattern();
                tablePattern.FrequencyList = new ObservableCollection<OperatingFrequency>();
                foreach (var f in e.FreqWork)
                    tablePattern.FrequencyList.Add(new OperatingFrequency()
                    {
                        FrequencyMin = f.FrequencyMin,
                        FrequencyMax = f.FrequencyMax,
                        Band = f.Band
                    });

                tablePattern.ImagePath = e.ImagePath;
                tablePattern.Name = e.Name;
                tablePattern.View = (byte)e.View;

                tablePattern.Id = e.Id;

                ChangePatternRecord(tablePattern);

            }
        }

        private void TemplateWindow_OnAddTemplateUAV(object sender, DBSourceCtrl.Models.PropertyTemplate e)
        {
            

            if (e != null)
            {
                TablePattern tablePattern = new TablePattern();
                tablePattern.FrequencyList = new ObservableCollection<OperatingFrequency>();
                foreach (var f in e.FreqWork)
                    tablePattern.FrequencyList.Add(new OperatingFrequency()
                    {
                        FrequencyMin = f.FrequencyMin,
                        FrequencyMax = f.FrequencyMax,
                        Band = f.Band
                    });

                tablePattern.ImagePath = e.ImagePath;
                tablePattern.Name = e.Name;
                tablePattern.View = (byte)e.View;

                AddPatternRecord(tablePattern);
            }
        }


     
        private void DefaultTemplates()
        {
            ClearPatternRecord();

            string fullPath = Path.GetFullPath(@"Templates");

            var rm = new System.Resources.ResourceManager(((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()).GetName().Name + ".Properties.Resources",
                ((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()));


          

            List<TablePattern> defaultTablePattern = new List<TablePattern>()
            {
            //0_Unknown

                new TablePattern()
                {
                     Id = 18,
                     Name = "Unknown",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 100,
                             FrequencyMax = 100,
                             Band = 1
                         }
                     },
                     ImagePath = fullPath+"/0_Unknown.png"



        },

            //1_DJI Phantom-4
                new TablePattern()
                {
                     Id = 1,
                     Name = "Phantom-4",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 5725,
                             FrequencyMax = 5850,
                             Band = 9
                         }
                     },
                     ImagePath = fullPath+"/1_DJI Phantom 4.png"

                },


            //2_DJI Mavic Air

                new TablePattern()
                {
                     Id = 2,
                     Name = "Mavic Air",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 2400,
                             FrequencyMax = 2480,
                             Band = 5
                         },

                         new OperatingFrequency()
                         {
                             FrequencyMin = 5725,
                             FrequencyMax = 5850,
                             Band = 5
                         }
                     },
                     ImagePath = fullPath+"/2_DJI Mavic Air.png"

                },

            //3_DJI Mavic 2

                new TablePattern()
                {
                     Id = 3,
                     Name = "Mavic 2",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 2400,
                             FrequencyMax = 2480,
                             Band = 10
                         },

                         new OperatingFrequency()
                         {
                             FrequencyMin = 5725,
                             FrequencyMax = 5850,
                             Band = 10
                         },
                         new OperatingFrequency()
                         {
                             FrequencyMin = 2400,
                             FrequencyMax = 2480,
                             Band = 20
                         },

                         new OperatingFrequency()
                         {
                             FrequencyMin = 5725,
                             FrequencyMax = 5850,
                             Band = 20
                         }
                     },
                     ImagePath = fullPath+"/3_DJI Mavic 2.png"
                },

             //4_Berkut

                new TablePattern()
                {
                     Id = 4,
                     Name = "Berkut",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 2330,
                             FrequencyMax = 2330,
                             Band = 20
                         }
                     },
                     ImagePath = fullPath+"/4_Berkut.png"
                },

            //5_Busel-M

                new TablePattern()
                {
                     Id = 5,
                     Name = "Busel-M",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 1100,
                             FrequencyMax = 1170,
                             Band = 9
                         }
                     },
                     ImagePath = fullPath+"/5_Busel-M.png"
                },

            //6_Orlan-10

                new TablePattern()
                {
                     Id = 6,
                     Name = "Orlan-10",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 1200,
                             FrequencyMax = 1270,
                             Band = 9
                         }
                     },
                     ImagePath = fullPath+"/6_Orlan-10.png"
                },

            //7_Orbiter-2A

                new TablePattern()
                {
                     Id = 7,
                     Name = "Orbiter-2A",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {                         
                         new OperatingFrequency()
                         {
                             FrequencyMin = 2290,
                             FrequencyMax = 2320,
                             Band = 30
                         }
                     },
                     ImagePath = fullPath+"/7_Orbiter-2A.png"
                },

            //8_RQ-11B Raven
               
                new TablePattern()
                {
                     Id = 8,
                     Name = "RQ-11B Raven",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 1100,
                             FrequencyMax = 1390,
                             Band = 5
                         },


                     },
                     ImagePath = fullPath+"/8_RQ-11B Raven.png"
                },

            //9_Skylark-1 LE

                new TablePattern()
                {
                     Id = 9,
                     Name = "Skylark-1 LE",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 1400,
                             FrequencyMax = 1690,
                             Band = 6
                         },
                     },
                     ImagePath = fullPath+"/9_Skylark-1 LE.png"


                },

            //10_RQ-11A Raven

                new TablePattern()
                {
                     Id = 10,
                     Name = "RQ-11A Raven",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 2200,
                             FrequencyMax = 2295,
                             Band = 7.5f
                         },
                     },
                     ImagePath = fullPath+"/10_RQ-11A Raven.png"
                },

            //11_Da-Vinci

                new TablePattern()
                {
                     Id = 11,
                     Name = "Da-Vinci",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 2320,
                             FrequencyMax = 2320,
                             Band = 5
                         },
                     },
                     ImagePath = fullPath+"/11_Da-Vinci.png"
                },

            //12_Orbiter-2M

                new TablePattern()
                {
                     Id = 12,
                     Name = "Orbiter-2M",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 4400,
                             FrequencyMax = 4690,
                             Band = 5
                         },
                     },
                     ImagePath = fullPath+"/12_Orbiter-2M.png"
                },


            //13_Schiebel

                new TablePattern()
                {
                     Id = 13,
                     Name = "Schiebel",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 4700,
                             FrequencyMax = 4800,
                             Band = 5
                         },
                     },
                     ImagePath = fullPath+"/13_Schiebel.png"
                },

            //14_Heron-1 TP

                new TablePattern()
                {
                     Id = 14,
                     Name = "Heron-1 TP",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 4900,
                             FrequencyMax = 5000,
                             Band = 5
                         },
                     },
                     ImagePath = fullPath+"/14_Heron-1 TP.png"
                },

            //15_Luna X2000

                new TablePattern()
                {
                     Id = 15,
                     Name = "Luna X2000",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 5400,
                             FrequencyMax = 5700,
                             Band = 5 //уточнить
                         },
                     },
                     ImagePath = fullPath+"/15_Luna X2000.png"
                },

            //16_3G
                new TablePattern()
                {
                     Id = 16,
                     Name = "3G",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 1920,
                             FrequencyMax = 1980,
                             Band = 5 //уточнить
                         },

                         new OperatingFrequency()
                         {
                             FrequencyMin = 2110 ,
                             FrequencyMax = 2170 ,
                             Band = 5
                         },

                         new OperatingFrequency()
                         {
                             FrequencyMin = 890,
                             FrequencyMax = 900,
                             Band = 5
                         },
                        new OperatingFrequency()
                         {
                             FrequencyMin = 935,
                             FrequencyMax = 960,
                             Band = 5
                         },
                        new OperatingFrequency()
                         {
                             FrequencyMin = 1710,
                             FrequencyMax = 1785,
                             Band = 5
                         },
                        new OperatingFrequency()
                         {
                             FrequencyMin = 1805,
                             FrequencyMax = 1880,
                             Band = 5
                         }
                     },
                     ImagePath = fullPath+"/16_3G.png"
                },

                //17_Orbiter-2B
                new TablePattern()
                {
                     Id = 17,
                     Name = "Orbiter-2B",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 4500,
                             FrequencyMax = 4700,
                             Band = 10
                         }
                         //new OperatingFrequency()
                         //{
                         //    FrequencyMin = 5065,
                         //    FrequencyMax = 5090,
                         //    Band = 10
                         //}
                     },
                     ImagePath = fullPath+"/7_Orbiter-2A.png"
                },




            //    // another UAV from table


            ////16_Phantom-2

            //    new TablePattern()
            //    {
            //         Id = 16,
            //         Name = "Phantom-2",
            //         FrequencyList = new ObservableCollection<OperatingFrequency>()
            //         {
            //             new OperatingFrequency()
            //             {
            //                 FrequencyMin = 5725,
            //                 FrequencyMax = 5850,
            //                 Band = 8
            //             }
            //         },
            //         ImagePath = fullPath+"/0_Unknown.png" // надо Phantom 2

            //    },

            ////17_Phantom-3

            //    new TablePattern()
            //    {
            //         Id = 17,
            //         Name = "Phantom-3",
            //         FrequencyList = new ObservableCollection<OperatingFrequency>()
            //         {
            //             new OperatingFrequency()
            //             {
            //                 FrequencyMin = 2400,
            //                 FrequencyMax = 2480,
            //                 Band = 9
            //             }
            //         },
            //         ImagePath = fullPath+"/0_Unknown.png" // надо Phantom 3
            //    },

            ////18_Phantom-4 Pro

            //    new TablePattern()
            //    {
            //         Id = 18,
            //         Name = "Phantom-4 Pro",
            //         FrequencyList = new ObservableCollection<OperatingFrequency>()
            //         {
            //             new OperatingFrequency()
            //             {
            //                 FrequencyMin = 2400,
            //                 FrequencyMax = 2480,
            //                 Band = 10
            //             },

            //             new OperatingFrequency()
            //             {
            //                 FrequencyMin = 5725,
            //                 FrequencyMax = 5850,
            //                 Band = 10
            //             },

            //             new OperatingFrequency()
            //             {
            //                 FrequencyMin = 2400,
            //                 FrequencyMax = 2480,
            //                 Band = 20
            //             },

            //             new OperatingFrequency()
            //             {
            //                 FrequencyMin = 5725,
            //                 FrequencyMax = 5850,
            //                 Band = 20
            //             }
            //         },
            //         ImagePath = fullPath+"/0_Unknown.png" // надо Phantom 3
            //    },


            // //19_Matrice-600

            //    new TablePattern()
            //    {
            //         Id = 19,
            //         Name = "Matrice-600",
            //         FrequencyList = new ObservableCollection<OperatingFrequency>()
            //         {
            //             new OperatingFrequency()
            //             {
            //                 FrequencyMin = 2400,
            //                 FrequencyMax = 2480,
            //                 Band = 9
            //             }
            //         },
            //         ImagePath = fullPath+"/0_Unknown.png" // надо Phantom 3
            //    },

            ////20_Phantom-3SE

            //    new TablePattern()
            //    {
            //         Id = 20,
            //         Name = "Phantom-3SE",
            //         FrequencyList = new ObservableCollection<OperatingFrequency>()
            //         {
            //             new OperatingFrequency()
            //             {
            //                 FrequencyMin = 2400,
            //                 FrequencyMax = 2480,
            //                 Band = 5
            //             },

            //             new OperatingFrequency()
            //             {
            //                 FrequencyMin = 5725,
            //                 FrequencyMax = 5850,
            //                 Band = 5
            //             }
            //         },
            //         ImagePath = fullPath+"/0_Unknown.png" // надо Phantom 3
            //    },

            ////21_SuperCam 350

            //    new TablePattern()
            //    {
            //         Id = 21,
            //         Name = "SuperCam 350",
            //         FrequencyList = new ObservableCollection<OperatingFrequency>()
            //         {
            //             new OperatingFrequency()
            //             {
            //                 FrequencyMin = 1215,
            //                 FrequencyMax = 1215,
            //                 Band = 10
            //             }
            //         },
            //         ImagePath = fullPath+"/0_Unknown.png" // надо Phantom 3
            //    },

            ////22_Orbitr-2B

            //    new TablePattern()
            //    {
            //         Id = 22,
            //         Name = "Orbitr-2B",
            //         FrequencyList = new ObservableCollection<OperatingFrequency>()
            //         {
            //             new OperatingFrequency()
            //             {
            //                 FrequencyMin = 4500,
            //                 FrequencyMax = 4600,
            //                 Band = 8
            //             }
            //         },
            //         ImagePath = fullPath+"/0_Unknown.png" // надо Phantom 3
            //    },

            ////23_Parrot

            //    new TablePattern()
            //    {
            //         Id = 23,
            //         Name = "Parrot",
            //         FrequencyList = new ObservableCollection<OperatingFrequency>()
            //         {
            //             new OperatingFrequency()
            //             {
            //                 FrequencyMin = 2340,
            //                 FrequencyMax = 2340,
            //                 Band = 5
            //             }
            //         },
            //         ImagePath = fullPath+"/0_Unknown.png" // надо Phantom 3
            //    }

            };

            AddPatternList(defaultTablePattern);

        }

    }
}