﻿using Bearing;
using GrozaS_AWS.Models;
using GrozaSModelsDBLib;
using OEM;
using RadarRodnikControl;
using Rodnik;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using WPFControlConnection;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        System.Timers.Timer tmrRodnikCheck = new System.Timers.Timer();

        private void RodnikControlConnection_ButServerClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (udpRodnik != null && udpRodnik.IsConnected)
                    DisconnectRodnik();
                else
                    ConnectRodnik();
            }

            catch
            { }
        }
        

        private void ConnectPortRodnik(object sender, EventArgs e)
        {
            mainWindowViewModel.StateConnectionRodnik = ConnectionStates.Connected;
            InitializeTimerRodnickCheck();
        }

        private void DisconnectPortRodnik(object sender, EventArgs e)
        {
            mainWindowViewModel.StateConnectionRodnik = ConnectionStates.Disconnected;
            DestructorTimerRodnickCheck();
        }

        private void UdpRodnik_OnReceiveMark(object sender, SPostMarkEventArgs e)
        {
            try
            {
                RodnikMark mark = DecodeMarkRodnik(e);

                if (mark != null)
                {
                    if (mark.Flag == FlagRodnik.Reset)
                        DeleteRodnikTarget(mark.ID);
                    else
                        if (rodnikTargets.Where(x => x.ID == mark.ID).ToList().Count == 0)
                        rodnikTargets.Add(new RodnikTarget { ID = mark.ID });

                        rodnikTargets.Where(x => x.ID == mark.ID).FirstOrDefault().rodnikMarks.Add(mark);

                    UpdateRodnikTargetsMap(rodnikTargets);


                    UpdateTableRodnik();
                }

                
            }
            catch 
            { }
        }

        

        private RodnikMark DecodeMarkRodnik(SPostMarkEventArgs e)
        {
            RodnikMark rodnik = null;

            try 
            {
                rodnik = new RodnikMark();

               
                rodnik.StartPosition = new GrozaSModelsDBLib.Coord()
                {
                    Latitude = mainWindowViewModel.LocalPropertiesVM.Rodnik.Latitude,
                    Longitude = mainWindowViewModel.LocalPropertiesVM.Rodnik.Longitude
                };


                rodnik.ID = (short)e.trackNum;
                rodnik.Azimuth = e.azimuth;
                rodnik.Range = e.range;
                rodnik.Velocity = e.velocity;
                rodnik.Aspect = e.aspect;
                rodnik.ExtrapCntr = (byte)e.extrapCntr;

                DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                rodnik.Loctime = start.AddMilliseconds(e.loctime).ToLocalTime();

               
                rodnik.Mark = (FlagRodnik)e.extrapMark;
                rodnik.Mode = (ModeRodnik)e.o_track_mode;
                rodnik.Flag = (FlagRodnik)e.o_track_flag;
                rodnik.Class = (ClassRodnik)e.rcgnClass;
                rodnik.Type = (TypeRodnik)e.o_type;
            }
            catch { }

            return rodnik;
        }

        private void DeleteRodnikTarget(short ID)
        {
            try
            {
                rodnikTargets.Remove(rodnikTargets.Where(x => x.ID == ID).FirstOrDefault());
            }
            catch { }
        }

        private void ClearRodnikTarget()
        {
            try
            {
                rodnikTargets.Clear();
            }
            catch { }
        }


        private void UpdateTableRodnik()
        {
            List<RodnikModel> listRodnikModels = new List<RodnikModel>();


            foreach (var rod in rodnikTargets)
            {
                listRodnikModels.Add(new RodnikModel()
                {
                    Id = rod.ID,
                    Azimuth = rod.rodnikMarks.Last().Azimuth,
                    Range = rod.rodnikMarks.Last().Range,
                    Aspect = rod.rodnikMarks.Last().Aspect,
                    Time = rod.rodnikMarks.Last().Loctime,
                    Class = Enum.GetName(typeof(ClassRodnik), rod.rodnikMarks.Last().Class), 
                    Velocity = rod.rodnikMarks.Last().Velocity,
                    Flag = Enum.GetName(typeof(FlagRodnik), rod.rodnikMarks.Last().Flag),                   
                    Mode = Enum.GetName(typeof(ModeRodnik), rod.rodnikMarks.Last().Mode)
                });
            }

            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    ucRodnik.ListRodnikModel = listRodnikModels;
                }), DispatcherPriority.Background);
            }
            catch
            { }


            
        }


        private void InitializeTimerRodnickCheck()
        {
            tmrRodnikCheck.Elapsed += TickTimerRodnikCheck;
            tmrRodnikCheck.Interval = 3000;

            tmrRodnikCheck.AutoReset = true;
            tmrRodnikCheck.Enabled = true;
        }

        private void DestructorTimerRodnickCheck()
        {
            tmrRodnikCheck.Elapsed -= TickTimerRodnikCheck;

            tmrRodnikCheck.Enabled = false;
        }

        void TickTimerRodnikCheck(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (rodnikTargets != null)
                {
                    DateTime dateTimeNow = new DateTime();
                    dateTimeNow = DateTime.Now;

                    foreach (var t in rodnikTargets)
                        if (dateTimeNow.Subtract(t.rodnikMarks.Last().Loctime).TotalSeconds > mainWindowViewModel.LocalPropertiesVM.Rodnik.LifeTime)
                        {
                            DeleteRodnikTarget(t.ID);
                            UpdateRodnikTargetsMap(rodnikTargets);
                            UpdateTableRodnik();
                        }
                            

                }    
            }
            catch { }

        }

        private void ucRodnik_OnDeleteRecord(object sender, RadarRodnikControl.RodnikModel e)
        {
            DeleteRodnikTarget(e.Id);
            UpdateTableRodnik();

            UpdateRodnikTargetsMap(rodnikTargets);
        }

        private void ucRodnik_OnClearRecords(object sender, EventArgs e)
        {
            ClearRodnikTarget();

            UpdateTableRodnik();

            UpdateRodnikTargetsMap(rodnikTargets);
        }

        private void ucRodnik_OnCentering(object sender, RadarRodnikControl.RodnikModel e)
        {
            Coord coordCenter = rodnikTargets.Where(x => x.ID == e.Id).FirstOrDefault().rodnikMarks.Last().Coordinate;

            mainWindowViewModel.LocationCenter = coordCenter;
        }
    }
}
