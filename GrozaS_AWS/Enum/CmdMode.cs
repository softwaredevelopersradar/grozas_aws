﻿//public enum Mode : byte
//{
//    STOP = 0,
//    ELINT = 1,
//    JAMMING = 2
//}




using System.ComponentModel;
using static GrozaS_AWS.MainWindow;

public enum Mode : byte
{
    [Description("MODE_STOP")]
    STOP = 0,

    [Description("MODE_ELINT")]    
    ELINT = 1,

    [Description("MODE_JAMMING")]    
    JAMMING = 2
}




