﻿using ClientDDF;
using GrozaS_AWS.TDF;
using System.Windows;
using TDF;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        public TCPClientDDF tcpClientDDF;

        

        private void ConnectDDF()
        {
            if (tcpClientDDF != null)
                DisconnectDDF();

            try
            {
                tcpClientDDF = new TCPClientDDF(mainWindowViewModel.LocalPropertiesVM.DF.IpAddress, mainWindowViewModel.LocalPropertiesVM.DF.Port);

                tcpClientDDF.OnDestroyServer += TcpClientDDF_OnDestroyServer;
                tcpClientDDF.OnConnect += TcpClientDDF_OnConnect;
                tcpClientDDF.OnDisconnect += TcpClientDDF_OnDisconnect;

                tcpClientDDF.OnMode += TcpClientDDF_OnMode;
                tcpClientDDF.OnParam += TcpClientDDF_OnParam;
                tcpClientDDF.OnSpectrum += TcpClientDDF_OnSpectrum;

                tcpClientDDF.Connect();
            }
            catch { }
        }

        private void TcpClientDDF_OnSpectrum(object sender, CommonModels.Model.SpectrumEventArgs e)
        {
            throw new System.NotImplementedException();
        }

        private void TcpClientDDF_OnParam(object sender, CommonModels.Model.ParamEventArgs e)
        {
            throw new System.NotImplementedException();
        }

        private void TcpClientDDF_OnMode(object sender, CommonModels.Model.ModeEventArgs e)
        {
            throw new System.NotImplementedException();
        }

        private void TcpClientDDF_OnDisconnect(object sender, System.EventArgs e)
        {
            throw new System.NotImplementedException();
        }

        private void TcpClientDDF_OnConnect(object sender, System.EventArgs e)
        {
            throw new System.NotImplementedException();
        }

        private void TcpClientDDF_OnDestroyServer(object sender, System.EventArgs e)
        {
            throw new System.NotImplementedException();
        }

        private void DisconnectDDF()
        {
            if (tcpClientTDF != null)
            {
                tcpClientTDF.Disconnect();

                tcpClientTDF.OnDestroy -= DestroyServerTDF;
                tcpClientTDF.OnConnect -= ConnectClientTDF;
                tcpClientTDF.OnDisconnect -= DisconnectClientTDF;

                tcpClientTDF.OnBearing -= BearingTDF;

                tcpClientTDF.OnChannel -= ChannelTDF;

                tcpClientTDF.OnCSource -= CSourceTDF;

                tcpClientTDF.OnSource -= SourceTDF;

                tcpClientTDF.OnRegime -= RegimeTDF;

                tcpClientTDF = null;
            }

        }

    }
}
