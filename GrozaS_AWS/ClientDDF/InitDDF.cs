﻿using ClientDDF;
using GrozaS_AWS.TDF;
using System.Windows;
using TDF;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        public TCPClientDDF tcpClientDDF;

        
        private void ConnectDDF()
        {
            if (tcpClientDDF != null)
                DisconnectDDF();

            try
            {
                tcpClientDDF = new TCPClientDDF(mainWindowViewModel.LocalPropertiesVM.DF.IpAddress, mainWindowViewModel.LocalPropertiesVM.DF.Port);

                tcpClientDDF.OnDestroyServer += DestroyServerDF;
                tcpClientDDF.OnConnect += ConnectClientDF;
                tcpClientDDF.OnDisconnect += DisconnectClientDF;

                ;


                tcpClientDDF.Connect();
            }
            catch { }
        }

        private void DisconnectDDF()
        {
            if (tcpClientDDF != null)
            {
                tcpClientDDF.Disconnect();

                tcpClientDDF.OnDestroyServer -= DestroyServerDF;
                tcpClientDDF.OnConnect -= ConnectClientDF;
                tcpClientDDF.OnDisconnect -= DisconnectClientDF;

             

                tcpClientDDF = null;
            }

        }

    }
}
