﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.Controllers
{
    public class ModelsProfile: Profile
    {

        public ModelsProfile()
        {
            CreateMap<DllGrozaSProperties.Models.CoordGloabal, GrozaSModelsDBLib.Coord>()
            .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.Latitude))
            .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.Longitude))
            .ForMember(dest => dest.Altitude, opt => opt.Ignore())
            .ReverseMap();

            CreateMap<DllGrozaSProperties.Models.Global.CmpGlobal, GrozaSModelsDBLib.ParamCmp>()
            
            .ForMember(dest => dest.Angle, opt => opt.MapFrom(src => src.Angle))
            .ReverseMap();


            CreateMap<DllGrozaSProperties.Models.Global.OemGlobal, GrozaSModelsDBLib.ParamOEM>()
            .ForMember(dest => dest.Distance, opt => opt.MapFrom(src => src.Distance))
            
            .ReverseMap();

            CreateMap<DllGrozaSProperties.Models.RadioIntelegence, GrozaSModelsDBLib.RadioIntelegence>()            
            .ForMember(dest => dest.DistanceIntelegence, opt => opt.MapFrom(src => src.Distance))
            .ForMember(dest => dest.BearingAccuracy, opt => opt.MapFrom(src => src.BearingAccuracy))
            .ForMember(dest => dest.ZoneAlarm, opt => opt.MapFrom(src => src.ZoneAlarm))
            .ForMember(dest => dest.ZoneReadiness, opt => opt.MapFrom(src => src.ZoneReadiness))
            .ForMember(dest => dest.ZoneAttention, opt => opt.MapFrom(src => src.ZoneAttention))
            .ForMember(dest => dest.StaticAngle, opt => opt.MapFrom(src => src.StaticAngle))
            .ReverseMap();


            CreateMap<DllGrozaSProperties.Models.Jamming, GrozaSModelsDBLib.Jamming>()
            .ForMember(dest => dest.TimeRadiat, opt => opt.MapFrom(src => src.TimeRadiat))
            .ForMember(dest => dest.Power100_500, opt => opt.MapFrom(src => src.Power100_500))
            .ForMember(dest => dest.Power500_2500, opt => opt.MapFrom(src => src.Power500_2500))
            .ForMember(dest => dest.Power2500_6000, opt => opt.MapFrom(src => src.Power2500_6000))
            .ForMember(dest => dest.DistanceJamming, opt => opt.MapFrom(src => src.Distance))
            .ForMember(dest => dest.Sector, opt => opt.MapFrom(src => src.Sector))
            .ReverseMap();
        }

    }

    public class ModelsVerseProfile : Profile
    {
        public ModelsVerseProfile()
        {
            CreateMap<GrozaSModelsDBLib.Coord, DllGrozaSProperties.Models.CoordGloabal>()
                 .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));

            //.ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.Latitude))
            //.ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.Longitude))
            //.ForMember(dest => dest.Altitude, opt => opt.Ignore())
            //.ReverseMap();

            CreateMap<GrozaSModelsDBLib.Coord, DllGrozaSProperties.Models.Global.CmpGlobal>()
                 .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));

            //.ForMember(dest => dest.ErrorDeg, opt => opt.MapFrom(src => src.ErrorDeg))
            //.ForMember(dest => dest.Angle, opt => opt.MapFrom(src => src.Angle))
            //.ReverseMap();


            CreateMap<GrozaSModelsDBLib.Coord, DllGrozaSProperties.Models.Global.OemGlobal>()
                 .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));

            //.ForMember(dest => dest.Distance, opt => opt.MapFrom(src => src.Distance))
            //.ForMember(dest => dest.ErrorDeg, opt => opt.MapFrom(src => src.ErrorDeg))
            //.ReverseMap();

            CreateMap<GrozaSModelsDBLib.Coord, DllGrozaSProperties.Models.RadioIntelegence>()
                 .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));

            //.ForMember(dest => dest.AutoCourse, opt => opt.MapFrom(src => src.AutoCourse))
            //.ForMember(dest => dest.CourseAngle, opt => opt.MapFrom(src => src.CourseAngle))
            //.ForMember(dest => dest.DistanceIntelegence, opt => opt.MapFrom(src => src.Distance))
            //.ForMember(dest => dest.BearingAccuracy, opt => opt.MapFrom(src => src.BearingAccuracy))
            //.ForMember(dest => dest.ZoneAlarm, opt => opt.MapFrom(src => src.ZoneAlarm))
            //.ForMember(dest => dest.ZoneReadiness, opt => opt.MapFrom(src => src.ZoneReadiness))
            //.ForMember(dest => dest.ZoneAttention, opt => opt.MapFrom(src => src.ZoneAttention))
            //.ForMember(dest => dest.StaticAngle, opt => opt.MapFrom(src => src.StaticAngle))
            //.ReverseMap();


            CreateMap<GrozaSModelsDBLib.Coord, DllGrozaSProperties.Models.Jamming>()
                 .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));

            //.ForMember(dest => dest.TimeRadiat, opt => opt.MapFrom(src => src.TimeRadiat))
            //.ForMember(dest => dest.Power100_500, opt => opt.MapFrom(src => src.Power100_500))
            //.ForMember(dest => dest.Power500_2500, opt => opt.MapFrom(src => src.Power500_2500))
            //.ForMember(dest => dest.Power2500_6000, opt => opt.MapFrom(src => src.Power2500_6000))
            //.ForMember(dest => dest.DistanceJamming, opt => opt.MapFrom(src => src.Distance))
            //.ForMember(dest => dest.Sector, opt => opt.MapFrom(src => src.Sector))
            //.ReverseMap();
        }
    }
}
