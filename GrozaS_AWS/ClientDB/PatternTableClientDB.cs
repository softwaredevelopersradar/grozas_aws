﻿using ClientDataBase.ServiceDB;
using InheritorsEventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using ClientDataBase;
using GrozaSModelsDBLib;
using System.Threading;
using TableEvents;
using UIMap;
using AutoMapper;
using System.Windows.Threading;
using WPFControlConnection;
using WpfMapControl;
using System.Collections.ObjectModel;
using GrozaS_AWS.Models;
using GrozaS_AWS.TDF;

namespace GrozaS_AWS
{
    public partial class MainWindow: Window
    {
        List<TablePattern> lTablePattern = new List<TablePattern>();

        private void AddPatternRecord(TablePattern tablePattern)
        {
            try
            {
                if (mainWindowViewModel.clientDB == null)
                    return;

                mainWindowViewModel.clientDB?.Tables[NameTable.TablePattern].Add(tablePattern);
            }
            catch
            { }
        }

        private void AddPatternList(List<TablePattern> listTablePattern)
        {
            try
            {
                if (mainWindowViewModel.clientDB == null)
                    return;

                mainWindowViewModel.clientDB?.Tables[NameTable.TablePattern].AddRangeAsync(listTablePattern);
            }
            catch
            { }
        }

        private void ClearPatternRecord()
        {
            try
            {
                if (mainWindowViewModel.clientDB == null)
                    return;

                mainWindowViewModel.clientDB?.Tables[NameTable.TablePattern].CLearAsync();
            }
            catch
            { }
        }

        private void ChangePatternRecord(TablePattern tablePattern)
        {
            if (mainWindowViewModel.clientDB != null)
            {
                //tablePattern.ImagePath = "";
                mainWindowViewModel.clientDB.Tables[NameTable.TablePattern].ChangeAsync(tablePattern);
            }
                

        }

        private void DeletePatternRecord(TablePattern tablePattern)
        {
            if (mainWindowViewModel.clientDB != null)
                mainWindowViewModel.clientDB.Tables[NameTable.TablePattern].DeleteAsync(tablePattern);
        }


    }
}
