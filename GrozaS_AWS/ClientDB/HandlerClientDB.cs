﻿using ClientDataBase.ServiceDB;
using InheritorsEventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using ClientDataBase;
using GrozaSModelsDBLib;
using System.Threading;
using TableEvents;
using UIMap;
using AutoMapper;
using System.Windows.Threading;
using WPFControlConnection;
using WpfMapControl;
using System.Collections.ObjectModel;
using GrozaS_AWS.Models;
using GrozaS_AWS.TDF;

namespace GrozaS_AWS
{
    public partial class MainWindow: Window
    {
       
        private void DBontrolConnection_ButServerClick(object sender, RoutedEventArgs e)
        {
            try                
            {
                if (mainWindowViewModel.clientDB != null )//&& mainWindowViewModel.clientDB.IsConnected())
                    DisconnectClientDB();
                else
                {
                    ConnectClientDB();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private void HandlerError_ClientDb(object sender, InheritorsEventArgs.OperationTableEventArgs e)
        {
            
            MessageBox.Show(e.GetMessage);
        }

        private void HandlerDisconnect_ClientDb(object sender, ClientEventArgs e)
        {
            mainWindowViewModel.StateConnectionDB = ConnectionStates.Disconnected;
        }

        private void HandlerConnect_ClientDb(object sender, ClientEventArgs e)
        {
            mainWindowViewModel.StateConnectionDB = ConnectionStates.Connected;

            LoadTables();
            
        }

        
        private async void LoadTablesByFilter(int id)
        { 
            if (mainWindowViewModel.clientDB != null)
            {
                try
                {

                    lFreqKnown = await (mainWindowViewModel.clientDB.Tables[NameTable.TableFreqKnown] as IDependentAsp).LoadByFilterAsync<FreqRanges>(id);
                    ucFreqKnown.UpdateFreqRangesS(lFreqKnown);

                    lFreqRangesRecon = await (mainWindowViewModel.clientDB.Tables[NameTable.TableFreqRangesRecon] as IDependentAsp).LoadByFilterAsync<FreqRanges>(id);
                    ucFreqRangesRecon.UpdateFreqRangesS(lFreqRangesRecon);

                    lFreqForbidden = await (mainWindowViewModel.clientDB.Tables[NameTable.TableFreqForbidden] as IDependentAsp).LoadByFilterAsync<FreqRanges>(id);
                    ucFreqForbidden.UpdateFreqRangesS(lFreqForbidden);

                    lSectorsRecon = await (mainWindowViewModel.clientDB.Tables[NameTable.TableSectorsRecon] as IDependentAsp).LoadByFilterAsync<TableSectorsRecon>(id);
                    ucSectorsRecon.UpdateSectorsRecon(lSectorsRecon);
                }
                catch (ClientDataBase.ExceptionClient exeptClient)
                {
                    MessageBox.Show(exeptClient.Message);
                }
                catch (ClientDataBase.ExceptionDatabase excpetService)
                {
                    MessageBox.Show(excpetService.Message);
                }
            }
            
        }

        private void HandlerUpdate_TableJammer(object sender, InheritorsEventArgs.TableEventArgs<TableJammerStation> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lJammerStation = e.Table;
                ucJammerStation.UpdateJammerStation(lJammerStation);
            });

            UpdateJammerMap(e.Table);

            UpdateMapBearing(lJammerStation, CurrentSourcesDF);

            UpdateSideMenu(lJammerStation);

            foreach (var source in CurrentSourcesDF)
                foreach (var track in source.Track)
                    track.Jammers = new ObservableCollection<TableJammerStation>(e.Table);
        }
            

        private void HandlerUpdate_TableFreqRangesRecon(object sender, InheritorsEventArgs.TableEventArgs<TableFreqRangesRecon> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lFreqRangesRecon = (from t in e.Table let a = t as FreqRanges select a).Where(x => x.NumberASP == PropNumJammerStation.SelectedNumJammerStation).ToList();
                ucFreqRangesRecon.UpdateFreqRangesS(lFreqRangesRecon);
            });
        }

        private void HandlerUpdate_TableFreqKnown(object sender, InheritorsEventArgs.TableEventArgs<TableFreqKnown> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lFreqKnown = (from t in e.Table let a = t as FreqRanges select a).Where(x => x.NumberASP == PropNumJammerStation.SelectedNumJammerStation).ToList();
                ucFreqKnown.UpdateFreqRangesS(lFreqKnown);
            });
        }

        private void HandlerUpdate_TableFreqForbidden(object sender, InheritorsEventArgs.TableEventArgs<TableFreqForbidden> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lFreqForbidden = (from t in e.Table let a = t as FreqRanges select a).Where(x => x.NumberASP == PropNumJammerStation.SelectedNumJammerStation).ToList();
                ucFreqForbidden.UpdateFreqRangesS(lFreqForbidden);
            });
        }

        private void HandlerUpdate_TableSectorsRecon(object sender, TableEventArgs<TableSectorsRecon> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lSectorsRecon = (from t in e.Table let a = t as TableSectorsRecon select a).Where(x => x.NumberASP == PropNumJammerStation.SelectedNumJammerStation).ToList();
                ucSectorsRecon.UpdateSectorsRecon(lSectorsRecon);
            });
        }

        private void HandlerUpdate_TableSource(object sender, InheritorsEventArgs.TableEventArgs<TableSource> e)
        {
            
            
            InitCurrentSourcesDF(new ObservableCollection<TableSource>(e.Table), new ObservableCollection<TableJammerStation>(lJammerStation));

            RefreshDependenceSource();

            CheckVoiceAlarm();

        }


        private void CurrentSourcesDFTrack_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
                RefreshDependenceSource();

            CheckVoiceAlarm();
        }

        private ObservableCollection<SourceDF> InitCurrentSourcesDF(ObservableCollection<TableSource> tableSource, ObservableCollection<TableJammerStation> tableJammerStations)
        {
            ObservableCollection<SourceDF> sourcesDF = new ObservableCollection<SourceDF>();
            foreach (var source in tableSource)
            {
                ObservableCollection<TrackPoint> tempTrack = new ObservableCollection<TrackPoint>();
                foreach (var track in source.Track)
                {

                    ObservableCollection<BearingItem> tempBearing = new ObservableCollection<BearingItem>();

                    foreach (var bear in track.Bearing)
                        tempBearing.Add(new BearingItem()
                        {
                            Bearing = bear.Bearing,
                            Distance = bear.Distance,
                            Jammer = bear.NumJammer,
                            ID = bear.Id
                        });



                    tempTrack.Add(new TrackPoint()
                    {
                        Time = track.Time,
                        Frequency = track.FrequencyMHz,
                        FrequencyRX = track.FrequencyRX,
                        Band = track.BandMHz,
                        Bearings = tempBearing,
                        Jammers = tableJammerStations,
                        ZoneValue = new List<Range>(3)
                        {
                            new Range(1,mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.ZoneAlarm),
                            new Range(mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.ZoneAlarm, mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.ZoneReadiness),
                            new Range(mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.ZoneReadiness, mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.ZoneAttention)
                        }



                    }); ; ;

                }


                sourcesDF.Add(new SourceDF()
                {
                    ID = source.Id,
                    Note = source.Note,
                    Type = source.Type,
                    Track = tempTrack,
                    TypeRSM = (TypeRSMart)(source.TypeRSM)
                    //TypeRSM = TypeRSMart.ENEMY


                });
            }

            
           
             
            CurrentSourcesDF.Clear();
            foreach (var s in sourcesDF)
                CurrentSourcesDF.Add(s);

            try
            {
                if (lastSource.Note != null && lastSource.ID != 0)
                CurrentSourcesDF.Where(x => x.ID == lastSource.ID).FirstOrDefault().Note = lastSource.Note;
                CurrentSourcesDF.Where(x => x.ID == lastSource.ID).FirstOrDefault().Type = lastSource.Type;
                CurrentSourcesDF.Where(x => x.ID == lastSource.ID).FirstOrDefault().Track = lastSource.Track;
            }
            catch
            { }

            foreach (var c in CurrentSourcesDF)
            {
                c.Track.CollectionChanged += CurrentSourcesDFTrack_CollectionChanged;
                c.PropertyChanged += C_PropertyChanged;
            }

            return sourcesDF;
        }

        private void C_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "TypeRSM":                    
                        RefreshDependenceSource();
                    break;

                default:
                    break;
            }
        }

        private void HandlerUpdate_TableSuppressSource(object sender, TableEventArgs<TableSuppressSource> e)
        {
            JammingDrone_UpdateSuppressSources(e.Table);
        }


        private void HandlerUpdate_TableSuppressGnss(object sender, TableEventArgs<TableSuppressGnss> e)
        {
            JammingDrone_UpdateSuppressGnss(e.Table);
        }
        

        private async void HandlerUpdate_GlobalProperties(object sender, InheritorsEventArgs.TableEventArgs<GlobalProperties> e)
        {
            InitGlobalProperties();
        }

        private async void HandlerUpdate_TablePattern(object sender, InheritorsEventArgs.TableEventArgs<TablePattern> e)
        {
            lTablePattern = await mainWindowViewModel.clientDB.Tables[NameTable.TablePattern].LoadAsync<TablePattern>();
            UpdateTablePattern();
            DroneControl_UpdateSourcesCollection();
        }

        private async void HandlerUpdate_TableOwnUAV(object sender, TableEventArgs<TableOwnUAV> e)
        {
            lOwnUAV = await mainWindowViewModel.clientDB.Tables[NameTable.TableOwnUAV].LoadAsync<TableOwnUAV>();

           
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                ucOwnUAV.UpdateOwnUAV(lOwnUAV);
            });

            if (flagUpdateSR == false)
                UpdateListOwnUAV();
            flagUpdateSR = false;
        }

        private async void LoadTables()
        {
            try
            {
                lJammerStation = await mainWindowViewModel.clientDB.Tables[NameTable.TableJammerStation].LoadAsync<TableJammerStation>();
                ucJammerStation.UpdateJammerStation(lJammerStation);
                UpdateSideMenu(lJammerStation);

                await InitGlobalProperties();

                UpdateCourseAngleCmpTX();

                lTablePattern = await mainWindowViewModel.clientDB.Tables[NameTable.TablePattern].LoadAsync<TablePattern>();

                UpdateTablePattern();
                DroneControl_UpdateSourcesCollection();

                UpdateJammerMap(lJammerStation);

                lSectorsRecon = await mainWindowViewModel.clientDB.Tables[NameTable.TableSectorsRecon].LoadAsync<TableSectorsRecon>();
                ucSectorsRecon.UpdateSectorsRecon(lSectorsRecon.Where(x => x.NumberASP == PropNumJammerStation.SelectedNumJammerStation).ToList());

                lFreqKnown = await mainWindowViewModel.clientDB.Tables[NameTable.TableFreqKnown].LoadAsync<FreqRanges>();
                ucFreqKnown.UpdateFreqRangesS(lFreqKnown.Where(x => x.NumberASP == PropNumJammerStation.SelectedNumJammerStation).ToList());

                lFreqRangesRecon = await mainWindowViewModel.clientDB.Tables[NameTable.TableFreqRangesRecon].LoadAsync<FreqRanges>();
                ucFreqRangesRecon.UpdateFreqRangesS(lFreqRangesRecon.Where(x => x.NumberASP == PropNumJammerStation.SelectedNumJammerStation).ToList());

                lFreqForbidden = await mainWindowViewModel.clientDB.Tables[NameTable.TableFreqForbidden].LoadAsync<FreqRanges>();
                ucFreqForbidden.UpdateFreqRangesS(lFreqForbidden.Where(x => x.NumberASP == PropNumJammerStation.SelectedNumJammerStation).ToList());

                lOwnUAV = await mainWindowViewModel.clientDB.Tables[NameTable.TableOwnUAV].LoadAsync<TableOwnUAV>();
                ucOwnUAV.UpdateOwnUAV(lOwnUAV);

                UpdateListOwnUAV();

                UpdateMapDirectionTX();
                UpdateMapDirectionRX();
                UpdateMapDirectionOEM(10);


                DroneControl_LoadDrones();
                JammingDrone_LoadSuppressSources();
                JammingDrone_LoadSuppressGnss();

                var tableSource = await mainWindowViewModel.clientDB.Tables[NameTable.TableSource].LoadAsync<TableSource>();

                InitCurrentSourcesDF(new ObservableCollection<TableSource>(tableSource), new ObservableCollection<TableJammerStation>(lJammerStation));

                ConnectLNK();


            }
            catch (ClientDataBase.ExceptionClient exeptClient)
            {
                MessageBox.Show(exeptClient.Message);
            }
            catch (ClientDataBase.ExceptionDatabase excpetService)
            {
                MessageBox.Show(excpetService.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AddSourceTDF_DB(TableSource tableSource)
        {
            try
            {
                if (mainWindowViewModel.clientDB == null)
                    return;

                mainWindowViewModel.clientDB?.Tables[NameTable.TableSource].Add(tableSource);
            }
            catch
            { }
        }

        private void ChangeSourceTDF_DB(TableSource tableSource)
        {
           
            try
            {
                if (mainWindowViewModel.clientDB == null)
                    return;

                mainWindowViewModel.clientDB?.Tables[NameTable.TableSource].ChangeAsync(tableSource);
            }
            catch
            { }
        }

        private void ClearSourceTDF_DB()
        {
            try
            {
                if (mainWindowViewModel.clientDB == null)
                    return;

                mainWindowViewModel.clientDB?.Tables[NameTable.TableSource].CLearAsync();
            }
            catch
            { }
            
        }

        private void DeleteSourceTDF_DB(int ID )
        {
           
            try
            {
                if (mainWindowViewModel.clientDB == null)
                    return;

                mainWindowViewModel.clientDB?.Tables[NameTable.TableSource].DeleteAsync(new TableSource() { Id = ID });
            }
            catch
            { }
        }

        private void SaveSourceTDF_DB()
        {            
            try
            {
                List<TableSource> tableSourceConvert = new List<TableSource>();

                foreach (var src in CurrentSourcesDF)
                {
                   
                    ObservableCollection<TableTrack> trackConvert = new ObservableCollection<TableTrack>();

                    foreach (var trc in src.Track)
                    {
                        ObservableCollection<TableJamBearing> bearingConvert = new ObservableCollection<TableJamBearing>();

                        foreach (var brn in trc.Bearings)
                        {
                            bearingConvert.Add(new TableJamBearing()
                            {
                                //Id = brn.ID,
                                Bearing = brn.Bearing,
                                Distance = brn.Distance,
                                NumJammer = brn.Jammer
                            });
                        }



                        trackConvert.Add(new TableTrack()
                        {
                            Id = trc.ID,
                            Time = trc.Time,
                            FrequencyMHz = trc.Frequency,
                            FrequencyRX = trc.FrequencyRX,
                            BandMHz = trc.Band,
                            Coordinates = trc.Coordinate,
                            Bearing = bearingConvert

                        });
                    }

                    tableSourceConvert.Add(new TableSource()
                    {
                        Id = src.ID,
                        Type = src.Type,
                        TypeRSM = (byte)src.TypeRSM,
                        Note = src.Note,
                        Track = trackConvert
                    });

                    

                };


                mainWindowViewModel.clientDB?.Tables[NameTable.TableSource].CLearAsync();

                mainWindowViewModel.clientDB?.Tables[NameTable.TableSource].AddRangeAsync(tableSourceConvert.ToList());
            }
            catch
            { }

        }
    }
}
