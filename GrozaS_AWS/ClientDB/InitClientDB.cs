﻿using ClientDataBase;
using GrozaSModelsDBLib;
using InheritorsEventArgs;
using System;
using System.Windows;


namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        
        string endPoint = "127.0.0.1:8302";

        //TODO: по номеру арма из настроек брать
        private new string Name { get; set; } = "ARM";

        private void ConnectClientDB()
        {
            if (mainWindowViewModel.clientDB != null)
                DisconnectClientDB();
            try
            {

                endPoint = mainWindowViewModel.LocalPropertiesVM.DB.IpAddress.ToString() + ":" + mainWindowViewModel.LocalPropertiesVM.DB.Port;
                mainWindowViewModel.clientDB = new ClientDB(this.Name, endPoint);

                mainWindowViewModel.clientDB.OnConnect += HandlerConnect_ClientDb;
                mainWindowViewModel.clientDB.OnDisconnect += HandlerDisconnect_ClientDb;
                mainWindowViewModel.clientDB.OnErrorDataBase += HandlerError_ClientDb;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableJammerStation] as ITableUpdate<TableJammerStation>).OnUpTable += HandlerUpdate_TableJammer;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableFreqRangesRecon] as ITableUpdate<TableFreqRangesRecon>).OnUpTable += HandlerUpdate_TableFreqRangesRecon;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableFreqKnown] as ITableUpdate<TableFreqKnown>).OnUpTable += HandlerUpdate_TableFreqKnown;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableFreqForbidden] as ITableUpdate<TableFreqForbidden>).OnUpTable += HandlerUpdate_TableFreqForbidden;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableSectorsRecon] as ITableUpdate<TableSectorsRecon>).OnUpTable += HandlerUpdate_TableSectorsRecon;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableSource] as ITableUpdate<TableSource>).OnUpTable += HandlerUpdate_TableSource;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableSuppressSource] as ITableUpdate<TableSuppressSource>).OnUpTable += HandlerUpdate_TableSuppressSource;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableSuppressGnss] as ITableUpdate<TableSuppressGnss>).OnUpTable += HandlerUpdate_TableSuppressGnss;
                (mainWindowViewModel.clientDB.Tables[NameTable.GlobalProperties] as ITableUpdate<GlobalProperties>).OnUpTable += HandlerUpdate_GlobalProperties;
                (mainWindowViewModel.clientDB.Tables[NameTable.TablePattern] as ITableUpdate<TablePattern>).OnUpTable += HandlerUpdate_TablePattern;

                (mainWindowViewModel.clientDB.Tables[NameTable.TableOwnUAV] as ITableUpdate<TableOwnUAV>).OnUpTable += HandlerUpdate_TableOwnUAV;

                mainWindowViewModel.clientDB.ConnectAsync();
            }
            catch{ }

            
        }

      

        private void DisconnectClientDB()
        {
            if (mainWindowViewModel.clientDB != null)
            {
                mainWindowViewModel.clientDB.Disconnect();

                mainWindowViewModel.clientDB.OnConnect -= HandlerConnect_ClientDb;
                mainWindowViewModel.clientDB.OnDisconnect -= HandlerDisconnect_ClientDb;
                mainWindowViewModel.clientDB.OnErrorDataBase -= HandlerError_ClientDb;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableJammerStation] as ITableUpdate<TableJammerStation>).OnUpTable -= HandlerUpdate_TableJammer;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableFreqRangesRecon] as ITableUpdate<TableFreqRangesRecon>).OnUpTable -= HandlerUpdate_TableFreqRangesRecon;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableFreqKnown] as ITableUpdate<TableFreqKnown>).OnUpTable -= HandlerUpdate_TableFreqKnown;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableFreqForbidden] as ITableUpdate<TableFreqForbidden>).OnUpTable -= HandlerUpdate_TableFreqForbidden;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableSectorsRecon] as ITableUpdate<TableSectorsRecon>).OnUpTable -= HandlerUpdate_TableSectorsRecon;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableSource] as ITableUpdate<TableSource>).OnUpTable -= HandlerUpdate_TableSource;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableSuppressSource] as ITableUpdate<TableSuppressSource>).OnUpTable -= HandlerUpdate_TableSuppressSource;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableSuppressGnss] as ITableUpdate<TableSuppressGnss>).OnUpTable -= HandlerUpdate_TableSuppressGnss;
                (mainWindowViewModel.clientDB.Tables[NameTable.GlobalProperties] as ITableUpdate<GlobalProperties>).OnUpTable -= HandlerUpdate_GlobalProperties;

                mainWindowViewModel.clientDB = null;
            }

        }


    }
}