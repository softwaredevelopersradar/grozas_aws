﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using ClientServerLib;
using WPFControlConnection;
using ToRinexConverter;

namespace GrozaS_AWS
{
    public partial class MainWindow
    {
        //private Thread almThread;
        //private Thread ephThread;
        private Thread coordThread;


        private void StartGnssThreads()
        {
            try
            {
                StopGnssThreads();

                clientGnss?.SendMessage(Operations.RecLocation);

                coordThread = new Thread(new ThreadStart(StartAutoRequestCoords));
                coordThread.IsBackground = true;
                coordThread.Start();

                //almThread = new Thread(new ThreadStart(StartAutoRequestAlmanac));
                //almThread.IsBackground = true;
                //almThread.Start();

                //ephThread = new Thread(new ThreadStart(StartAutoRequestEphemeris));
                //ephThread.IsBackground = true;
                //ephThread.Start();
            }
            catch { }
        }


        private void StopGnssThreads()
        {
            try
            {
                coordThread?.Abort();
                //almThread?.Abort();
                //ephThread?.Abort();

                coordThread = null;
                //almThread = null;
                //ephThread = null;
            }
            catch { }
        }
        

        //private void StartAutoRequestAlmanac()
        //{
        //    try
        //    {
        //        if (mainWindowViewModel.LocalPropertiesVM.GNSS.IntervalRequestAlmanac == 0)
        //        {
        //            clientGnss?.SendMessage(Operations.Almanac);
        //            mainWindowViewModel.LocalPropertiesVM.GNSS.AutoRequestAlmanac = false;
        //            SetLocalProperties();
        //            return;
        //        }

        //        while (mainWindowViewModel.LocalPropertiesVM.GNSS.AutoRequestAlmanac)
        //        {
        //            clientGnss?.SendMessage(Operations.Almanac);
        //            Thread.Sleep(mainWindowViewModel.LocalPropertiesVM.GNSS.IntervalRequestAlmanac * 60000);
        //        }
        //    }
        //    catch { }
        //}


        //private void StartAutoRequestEphemeris()
        //{
        //    try
        //    {
        //        if (mainWindowViewModel.LocalPropertiesVM.GNSS.IntervalRequestEphemeris == 0)
        //        {
        //            clientGnss?.SendMessage(Operations.Ephemeries);
        //            mainWindowViewModel.LocalPropertiesVM.GNSS.AutoRequestEphemeris = false;
        //            SetLocalProperties();
        //            return;
        //        }

        //        while (mainWindowViewModel.LocalPropertiesVM.GNSS.AutoRequestEphemeris)
        //        {
        //            clientGnss?.SendMessage(Operations.Ephemeries);
        //            Thread.Sleep(mainWindowViewModel.LocalPropertiesVM.GNSS.IntervalRequestEphemeris * 60000);
        //        }
        //    }
        //    catch { }
        //}


        private void StartAutoRequestCoords()
        {
            try
            {
                if (mainWindowViewModel.LocalPropertiesVM.GNSS.IntervalRequestCoordinate == 0)
                {
                    clientGnss?.SendMessage(Operations.RecLocation);
                    mainWindowViewModel.LocalPropertiesVM.GNSS.AutoRequestCoordinate = false;
                    SetLocalProperties();
                    return;
                }

                while (mainWindowViewModel.LocalPropertiesVM.GNSS.AutoRequestCoordinate)
                {
                    clientGnss?.SendMessage(Operations.RecLocation);
                    Thread.Sleep(mainWindowViewModel.LocalPropertiesVM.GNSS.IntervalRequestCoordinate * 60000);
                }
            }
            catch
            { }
        }


        private void HandlerError_ClientGnss(object sender, string e)
        {
            //MessageBox.Show(e);
        }


        private void HandlerReceiveData_ClientGnss(object sender, MyEventArgsGetData e)
        {
            try
            {
                switch (e.Command)
                {
                    //case 1:
                    //    if (clientGnss.ephemeries.Count == 32)
                    //    {
                    //        EphemerisConverter.fileName = "EphemerisRINEX.19n";
                    //        ephemerisConverter.WriteToRinex(mainWindowViewModel.LocalPropertiesVM.GNSS.FileEphemeris, clientGnss.ephemeries, 1);
                    //    }
                    //    else
                    //    {
                    //        MessageBox.Show("Ephemeris collection not complete");
                    //    }
                    //    break;

                    //case 2:

                    //    if (clientGnss.almanac.Count == 32)
                    //    {
                    //        AlmanacConverter.fileName = "AlmanacSEM.AL3";
                    //        almanacConverter.WriteToSEM(mainWindowViewModel.LocalPropertiesVM.GNSS.FileAlmanac, clientGnss.almanac, 1);
                    //    }
                    //    else
                    //    {
                    //        MessageBox.Show("Almanac collection not complete");
                    //    }
                    //    break;

                    case 5:
                        if (mainWindowViewModel.GlobalPropertiesVM != null)
                        {
                            mainWindowViewModel.GlobalPropertiesVM.Gnss.Latitude = clientGnss.reclocation.ReceiverLatitude;
                            mainWindowViewModel.GlobalPropertiesVM.Gnss.Longitude = clientGnss.reclocation.ReceiverLongitude;
                            mainWindowViewModel.GlobalPropertiesVM.Gnss.Altitude = (float)clientGnss.reclocation.ReceiverAltitude;


                        }
                        break;
                    default:
                        break;
                }

            }
            catch 
            { }
        }


        private void HandlerDisconnected_ClientGnss(object sender, bool result)
        {
            mainWindowViewModel.StateConnectionGNSS = ConnectionStates.Disconnected;
        }


        private void HandlerConnected_ClientGnss(object sender, bool result)
        {
            if (result)
            {
                mainWindowViewModel.StateConnectionGNSS = ConnectionStates.Connected;
            }
            else
            {
                mainWindowViewModel.StateConnectionGNSS = ConnectionStates.Disconnected;
            }
        }
    }
}
