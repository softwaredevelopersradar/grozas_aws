﻿using System.Windows;
using ClientLib;
using ToRinexConverter;

namespace GrozaS_AWS
{
    public partial class MainWindow
    {
        private Client clientGnss;
        private AlmanacConverter almanacConverter;
        private EphemerisConverter ephemerisConverter;


        private void GNSSControlConnection_ButServerClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (clientGnss != null)
                { 
                    DisconnectClientGnss(); 
                }
                else
                {
                    ConnectClientGnss();
                }
            }
            catch
            { }
        }

        
        private void ConnectClientGnss()
        {
            if (clientGnss != null)
                DisconnectClientGnss();

            try
            {

                clientGnss = new Client(mainWindowViewModel.LocalPropertiesVM.GNSS.IpAddress, mainWindowViewModel.LocalPropertiesVM.GNSS.Port);
                almanacConverter = new AlmanacConverter();
                ephemerisConverter = new EphemerisConverter();

                clientGnss.OnConnected += HandlerConnected_ClientGnss;
                clientGnss.OnDisconnected += HandlerDisconnected_ClientGnss;
                clientGnss.OnGetData += HandlerReceiveData_ClientGnss;
                clientGnss.OnError += HandlerError_ClientGnss;

                clientGnss.Connect();

                StartGnssThreads();
            }
            catch
            { }
        }


        private void DisconnectClientGnss()
        {
            if (clientGnss == null)
                return;

            clientGnss.Disconnect();
            clientGnss.OnConnected -= HandlerConnected_ClientGnss;
            clientGnss.OnDisconnected -= HandlerDisconnected_ClientGnss;
            clientGnss.OnGetData -= HandlerReceiveData_ClientGnss;
            clientGnss.OnError -= HandlerError_ClientGnss;

            clientGnss = null;
            almanacConverter = null;
            ephemerisConverter = null;
            StopGnssThreads();
        }
        
    }
}
