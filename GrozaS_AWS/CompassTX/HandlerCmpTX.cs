﻿using OEM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WPFControlConnection;
using static DLL_Compass.Compass;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        
        private void CmpTXControlConnection_ButServerClick(object sender, RoutedEventArgs e)
        {            
            try
            {
                if (compassTX != null)
                    DisconnectCmpTX();
                else
                    ConnectCmpTX();
            }

            catch
            { }

            
        }

        private void OpenPortCmpTX()
        {
            mainWindowViewModel.StateConnectionCmpTX = ConnectionStates.Connected;



        }

        private void ClosePortCmpTX()
        {
            mainWindowViewModel.StateConnectionCmpTX = ConnectionStates.Disconnected;
        }

        private void ReadByteCmpTX(byte[] data)
        {
            
        }

        private void WriteByteCmpTX(byte[] data)
        {
            
        }


        private void GetCompassTX()
        {
            if (compassTX != null)
                try
                {

                    //Random rnd = new Random();

                    //mainWindowViewModel.GlobalPropertiesVM.CmpTX.Angle = rnd.Next(0, 359);

                    mainWindowViewModel.GlobalPropertiesVM.CmpTX.Angle = compassTX._angles.head;

                }
                catch { }


        }

    }
}
