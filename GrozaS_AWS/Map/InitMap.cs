﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using GrozaS_AWS;
using GrozaS_AWS.Models;
using GrozaSModelsDBLib;
using UIMap;
using UIMap.Models;
using WpfMapControl;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
       

        private void UpdateJammerMap(List<TableJammerStation> lJammer)
        {

            JammersDB = new ObservableCollection<VMJammer>();

            foreach (TableJammerStation t in lJammer)
                JammersDB.Add(new VMJammer(t));


            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                RastrMap.Jammers.Clear();
                RastrMap.Jammers = JammersDB;
            });


            UpdateMapDirectionRX();

            UpdateMapDirectionTX();

            UpdateMapDirectionOEM(10);
        }

       
        private void UpdateSourceMap(ObservableCollection<SourceDF> sourceDF)
        {
            try
            {

                ObservableCollection<VMDrone> DroneDB = new ObservableCollection<VMDrone>();

                ObservableCollection<SourceDF> lSourceTemp = sourceDF;

                foreach (var t in lSourceTemp)
                {
                    string Type = TypeCodeCollection.ContainsKey(t.Type) ? TypeCodeCollection[t.Type] : "";
                    List<Location> locTrack = new List<Location>();
                    foreach (var tr in t.Track)
                    {
                        if (tr.Coordinate.Latitude !=0 && tr.Coordinate.Longitude !=0)
                            locTrack.Add(new Location(tr.Coordinate.Latitude, tr.Coordinate.Longitude));
                    }

                    float dist = 0;

                    try
                    {
                        int ownJammer = lJammerStation.Where(x => x.Role == StationRole.Own).FirstOrDefault().Id;
                        dist = t.Track.Last().Bearings.Where(x => x.Jammer == ownJammer).FirstOrDefault().Distance;
                    }
                    catch
                    { }


                    ViewUAV tempView = new ViewUAV();

                    if (t.Type == 0)
                        tempView = ViewUAV.Unknown;
                    else
                    {
                        if (t.Type == 16)
                            tempView = ViewUAV.GSM;
                        else
                        {
                            if (t.Type == 1 || t.Type == 2 || t.Type == 3)
                                tempView = ViewUAV.Multicopter;
                            else
                                tempView = ViewUAV.Plane;
                        }
                        
                    }
                    

                    DroneDB.Add(new VMDrone()
                    {
                        IDSource = t.ID,
                        Name = Type,
                        View = tempView,
                        LocationDrone = new Location(t.Track.Last().Coordinate.Longitude, t.Track.Last().Coordinate.Latitude, 0),
                        LocationTrack = locTrack,
                        Distance = dist,
                        Zone = (byte)t.Track.Last().Zone
                    }) ;
                }






                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    RastrMap.Drones.Clear();
                    RastrMap.Drones = DroneDB;
                });

            }
            catch { }


        }


        private void UpdateMapBearing(List <TableJammerStation> tableJammerStation, ObservableCollection<SourceDF> tableSource)
        {
            try
            {
                foreach (var jammer in tableJammerStation)
                {
                    ObservableCollection<VMBearing> vmbearing = new ObservableCollection<VMBearing>();
                    foreach (var source in tableSource)
                    {
                        if (source.Track.Last().Bearings.Where(x => x.Jammer == jammer.Id).ToList().Count > 0)
                        {

                            vmbearing.Add(new VMBearing(jammer.Coordinates)
                            {
                                Direction = source.Track.Last().Bearings.Where(x => x.Jammer == jammer.Id).Last().Bearing,
                                //Radius = mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.DistanceIntelegence,
                                Radius = mainWindowViewModel.LocalPropertiesVM.Map.DistanceBearingLine,
                                Visible = true,
                                IsSelected = (source.ID == SelectedID) ? true : false,
                                //IDSource = source.Track.Last().Bearings.Where(x => x.Jammer == jammer.Id).Last().TableSourceId
                                IDSource = source.ID
                            });
                        }
                    }
                

                
                    Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    if (RastrMap.Jammers != null && RastrMap.Jammers.Count > 0)
                    {
                        RastrMap.Jammers.Where(x => x.Id == jammer.Id).FirstOrDefault().BearingSource.Clear();
                        foreach (var bb in vmbearing)
                            RastrMap.Jammers.Where(x => x.Id == jammer.Id).FirstOrDefault().BearingSource.Add(bb);
                    }
                        
                });

                }

                
            }
            catch { }

        }

        private void UpdateMapDirectionTX()
        {
            try
            {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    if (RastrMap.Jammers != null && RastrMap.Jammers.Count > 0 && RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault() != null)
                    {

                        RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneJamming.Direction = mainWindowViewModel.DirectionTX.Result;
                        RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneJamming.Visible = true;
                        RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotate.Visible = (Math.Abs(mainWindowViewModel.DirectionTX.Result - mainWindowViewModel.DirectionBRD.Result) < 3) ? false : true;

                    }
                });
            }
            catch { }

           
        }

        private void UpdateMapDirectionRX()
        {
            try
            {

                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    if (RastrMap.Jammers != null && RastrMap.Jammers.Count > 0 && RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault() != null)                        
                        RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneBearing.Direction = mainWindowViewModel.DirectionRX.Result;
                });
            }
            catch { }
        }

        private void UpdateMapDirectionOEM(float sector)
        {
            try
            {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    if (RastrMap.Jammers != null && RastrMap.Jammers.Count > 0 && RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault() != null)
                    {
                        RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneOptic.Direction = mainWindowViewModel.DirectionOEM.Result;

                        if (sector > 60)
                        RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneOptic.Sector = sector;
                        
                    }
                });
            }
            catch { }
        }

        private void UpdateMapDirectionBRD()
        {
            try
            {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                if (RastrMap.Jammers != null && RastrMap.Jammers.Count > 0 && RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault() != null)
                {
                    
                    RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotate.Direction = mainWindowViewModel.DirectionBRD.Result;                   
                    RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotate.Visible = (Math.Abs(mainWindowViewModel.DirectionTX.Result - mainWindowViewModel.DirectionBRD.Result) < 3) ? false : true;
                }
            });
            }
            catch { }
        }


        private void HandlerDirectionEvent(object sender, float e)
        {

            SetAngleBRD(e);

            SetAngleOEM(e);

        }



        private void UpdateRodnikTargetsMap(ObservableCollection<RodnikTarget> rodnik)
        {
            try
            {

                ObservableCollection<VMRodnikTarget> RodnikTarget = new ObservableCollection<VMRodnikTarget>();

                foreach (var t in rodnik)
                {
                    
                    List<Location> locTrack = new List<Location>();
                    foreach (var tr in t.rodnikMarks)
                    {
                        if (tr.Coordinate.Latitude != -255 && tr.Coordinate.Longitude != -255)
                            locTrack.Add(new Location(tr.Coordinate.Latitude, tr.Coordinate.Longitude));
                    }

                    Random rnd = new Random();

                    
                    RodnikTarget.Add(new VMRodnikTarget()
                    {
                        IDSource = t.ID,
                        Name = t.ID.ToString(),
                        View = (ViewRodnik)t.rodnikMarks.Last().Class,
                        LocationDrone = new Location(t.rodnikMarks.Last().Coordinate.Longitude, t.rodnikMarks.Last().Coordinate.Latitude, 0),
                        LocationTrack = locTrack

                    }); ;
                }






                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    RastrMap.RodnikTargets.Clear();
                    RastrMap.RodnikTargets = RodnikTarget;
                });

            }
            catch { }


        }


        private void UpdateZorkiRTargetsMap(ObservableCollection<ZorkiRTarget> zorkiR)
        {
            try
            {

                ObservableCollection<VMZorkiRTarget> ZorkiRTarget = new ObservableCollection<VMZorkiRTarget>();

                foreach (var t in zorkiR)
                {

                    List<Location> locTrack = new List<Location>();
                    foreach (var tr in t.zorkiRMarks)
                    {
                        if (tr.Coordinate.Latitude != -255 && tr.Coordinate.Longitude != -255)
                            locTrack.Add(new Location(tr.Coordinate.Latitude, tr.Coordinate.Longitude));
                    }

                    Random rnd = new Random();


                    ZorkiRTarget.Add(new VMZorkiRTarget()
                    {
                        IDSource = t.ID,
                        Name = t.ID.ToString(),
                        View = (ViewZorkiR)t.zorkiRMarks.Last().Class,
                        LocationDrone = new Location(t.zorkiRMarks.Last().Coordinate.Longitude, t.zorkiRMarks.Last().Coordinate.Latitude, 0),
                        LocationTrack = locTrack

                    }); ;
                }






                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    RastrMap.ZorkiRTargets.Clear();
                    RastrMap.ZorkiRTargets = ZorkiRTarget;
                });

            }
            catch { }


        }
    }
}