﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace GrozaS_AWS.Models
{
    public class ExBearing:INotifyPropertyChanged
    {
        private TypeQuery _query;
        public TypeQuery Query
        {
            get { return _query; }
            set
            {
                if (_query.Equals(value)) return;
                _query = value;
                OnPropertyChanged();
            }

        }


        private int _id;
        public int ID
        {
            get { return _id; }
            set
            {
                if (_id.Equals(value)) return;
                _id = value;
                OnPropertyChanged();
            }

        }


        private byte _stationNumber;
        public byte StationNumber
        {
            get { return _stationNumber; }
            set
            {
                if (_stationNumber.Equals(value)) return;
                _stationNumber = value;
                OnPropertyChanged();
            }

        }


        private float _bearing;
        public float Bearing
        {
            get { return _bearing; }
            set
            {
                if (_bearing.Equals(value)) return;
                _bearing = value;
                OnPropertyChanged();
            }

        }

        private double _frequency;
        public double Frequency
        {
            get { return _frequency; }
            set
            {
                if (_frequency.Equals(value)) return;
                _frequency = value;
                OnPropertyChanged();
                
            }

        }

        private DateTime _time;
        public DateTime Time
        {
            get { return _time; }
            set
            {
                if (_time.Equals(value)) return;
                _time = value;
                OnPropertyChanged();

            }

        }

        private DateTime _timeAsk;
        public DateTime TimeAsk
        {
            get { return _timeAsk; }
            set
            {
                if (_timeAsk.Equals(value)) return;
                _timeAsk = value;
                OnPropertyChanged();

            }

        }

        public void Clear()
        {
            Query = TypeQuery.ANSWERNO;
            //ID = -1;
            //StationNumber = 0;
            Frequency = 0;
            Bearing = 0;
            
        }

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
        #endregion


    }
}
