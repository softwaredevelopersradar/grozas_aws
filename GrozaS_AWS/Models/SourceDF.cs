﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using GrozaSModelsDBLib;

namespace GrozaS_AWS.Models
{
    public class SourceDF:INotifyPropertyChanged
    {
        private int _id;
        public int ID
        {
            get { return _id; }
            set
            {
                if (_id.Equals(value)) return;
                _id = value;
                OnPropertyChanged();
            }

        }

        private string _note = "";

        public string Note 
        {
            get { return _note; }
            set
            {
                if (_note.Equals(value)) return;
                _note = value;
                OnPropertyChanged();
            }

        }


        private byte _type;
        public byte Type
        {
            get { return _type; }
            set
            {
                if (_type.Equals(value)) return;
                _type = value;
                OnPropertyChanged();
            }

        }

        


        private ObservableCollection<TrackPoint> _track = new ObservableCollection<TrackPoint>();
        public ObservableCollection<TrackPoint> Track
        {
            get { return _track; }
            set
            {
                if (_track.Equals(value)) 
                    return;

                _track = value;
                OnPropertyChanged();
            }

        }


        private TypeRSMart _typeRSM;
        public TypeRSMart TypeRSM
        {
            get { return _typeRSM; }
            set
            {
                if (_typeRSM.Equals(value)) return;
                _typeRSM = value;
                OnPropertyChanged();
            }

        }



        public SourceDF()
        {
            
        }


        


        public void AddTrackPoint(TrackPoint trackPoint)
        {
            try 
            {
                if (trackPoint.Frequency == 0)
                {
                    trackPoint.Frequency = Track.Last().Frequency;
                    trackPoint.Band = Track.Last().Band;
                    trackPoint.Time = DateTime.Now;

                    foreach (var bo in Track.Last().Bearings)
                        if (!trackPoint.Bearings.Any(x => x.Jammer == bo.Jammer))
                        {
                            bo.Distance = 0;
                            trackPoint.Bearings.Add(bo);
                        }
                            

                }

                Track.Add(trackPoint);


                if (Track.Count > 30)
                    for (int i = 0; i < Track.Count - 30; i++)
                        Track.RemoveAt(i);
            }
            catch { }

            
        }

        public void UpdateTrackPoint(BearingItem bearingItem)
        {            
            if (Track.Last().Bearings.Any(p => p.Jammer == bearingItem.Jammer))
                Track.Last().Bearings.Remove(Track.Last().Bearings.Where(p => p.Jammer == bearingItem.Jammer).FirstOrDefault());

            Track.Last().Bearings.Add(bearingItem);

        }

        
       

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
        #endregion

    }
}
