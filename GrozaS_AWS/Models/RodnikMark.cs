﻿using Bearing;
using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.Models
{
    public class RodnikMark
    {
        


        private short _iD;

        public short ID
        {
            get { return _iD; }
            set
            {
                if (_iD.Equals(value)) return;
                _iD = value;

            }
        }

        private float _azimuth=-1;

        public float Azimuth
        {
            get { return _azimuth; }
            set
            {
                if (_azimuth.Equals(value)) return;
                _azimuth = value;

                DefineCoord();

            }
        }


        private float _range=0 ;

        public float Range
        {
            get { return _range; }
            set
            {
                if (_range.Equals(value)) return;
                _range = value;

                DefineCoord();

            }
        }

        private float _velocity;

        public float Velocity
        {
            get { return _velocity; }
            set
            {
                if (_velocity.Equals(value)) return;
                _velocity = value;

            }
        }


        private float _aspect;

        public float Aspect
        {
            get { return _aspect; }
            set
            {
                if (_aspect.Equals(value)) return;
                _aspect = value;

            }
        }

        private DateTime _loctime;

        public DateTime Loctime
        {
            get { return _loctime; }
            set
            {
                if (_loctime.Equals(value)) return;
                _loctime = value;

            }
        }

        private byte _extrapCntr;

        public byte ExtrapCntr
        {
            get { return _extrapCntr; }
            set
            {
                if (_extrapCntr.Equals(value)) return;
                _extrapCntr = value;

            }
        }

        private TypeRodnik _type;

        public TypeRodnik Type
        {
            get { return _type; }
            set
            {
                if (_type.Equals(value)) return;
                _type = value;

            }
        }

        private ModeRodnik _mode;

        public ModeRodnik Mode
        {
            get { return _mode; }
            set
            {
                if (_mode.Equals(value)) return;
                _mode = value;

            }
        }

        private FlagRodnik _flag;

        public FlagRodnik Flag
        {
            get { return _flag; }
            set
            {
                if (_flag.Equals(value)) return;
                _flag = value;

            }
        }

        private FlagRodnik _mark;

        public FlagRodnik Mark
        {
            get { return _mark; }
            set
            {
                if (_mark.Equals(value)) return;
                _mark = value;

            }
        }

        private ClassRodnik _class;

        public ClassRodnik Class
        {
            get { return _class; }
            set
            {
                if (_class.Equals(value)) return;
                _class = value;

            }
        }



        private Coord _startPosition = new Coord();

        public Coord StartPosition
        {
            get { return _startPosition; }
            set
            {
                if (_startPosition.Equals(value)) return;
                _startPosition = value;

            }
        }



        private Coord _coordinate = new Coord();

        public Coord Coordinate
        {
            get { return _coordinate; }
            private set
            {
                if (_coordinate.Equals(value)) return;
                _coordinate = value;

            }
        }



        private void DefineCoord()
        {
            double tempLat = -255;
            double tempLong =-255;

            try 
            {
                if (Range > 0 && Azimuth > -1)
                    ClassBearing.f_Bearing(Azimuth,
                            Range, 300,
                            StartPosition.Latitude,
                            StartPosition.Longitude,
                            1,
                            ref tempLat, ref tempLong);

                
            }
            catch { }

            Coordinate = new Coord( ){ Latitude = tempLat, Longitude = tempLong };
        }


        public RodnikMark()
        {
           
        }

    }
}
