﻿using BRD;
using System.Windows;


namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        public ComBRD comBRD;

        private void ConnectBRD()
        {
            if (comBRD != null)
                DisconnectBRD();

            comBRD = new ComBRD((byte)mainWindowViewModel.LocalPropertiesVM.BRD.Address);

          
            comBRD.OnOpenPort += OpenPortBRD;
            comBRD.OnClosePort += ClosePortBRD;          
            comBRD.OnEcho += EchoBRD;
            comBRD.OnFreeRotate += FreeRotateBRD;
            comBRD.OnGetAngle += GetAngleBRD;
            comBRD.OnSetAngle += SetAngleBRD;
            comBRD.OnStop += StopBRD;

            comBRD.OpenPort(mainWindowViewModel.LocalPropertiesVM.BRD.ComPort, 19200, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);

            comBRD.Calibration();
        }

        private void DisconnectBRD()
        {
            if (comBRD != null)
            {
                comBRD.ClosePort();

                comBRD.OnOpenPort -= OpenPortBRD;
                comBRD.OnClosePort -= ClosePortBRD;
                comBRD.OnEcho -= EchoBRD;
                comBRD.OnFreeRotate -= FreeRotateBRD;
                comBRD.OnGetAngle -= GetAngleBRD;
                comBRD.OnSetAngle -= SetAngleBRD;
                comBRD.OnStop -= StopBRD;

                comBRD = null;
            }

        }

    }
}
