﻿
using BRD.Events;
using GrozaS_AWS.Models;
using GrozaSModelsDBLib;
using System;
using System.Threading;
using System.Windows;
using WPFControlConnection;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        private void BRDControlConnection_ButServerClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (comBRD != null)
                    DisconnectBRD();
                else
                {
                    ConnectBRD();
                }
            }
            catch
            { }
        }

        private void OpenPortBRD(object sender, EventArgs e)
        {
            mainWindowViewModel.StateConnectionBRD = ConnectionStates.Connected;
        }

        private void ClosePortBRD(object sender, EventArgs e)
        {
            mainWindowViewModel.StateConnectionBRD = ConnectionStates.Disconnected;
        }

        private void EchoBRD(object sender, ErrorEventArgs e)
        { }

        private void FreeRotateBRD(object sender, ErrorEventArgs e)
        { }

        private void GetAngleBRD(object sender, AngleEventArgs e)
        {
            mainWindowViewModel.DirectionTX.Azimuth = e.Angle;



        }

        private void SetAngleBRD(object sender, ErrorEventArgs e)
        { }

        private void StopBRD(object sender, ErrorEventArgs e)
        { }

        private void HandlerJammerPositionEvent(object sender, WpfMapControl.Location e)
        {
            try
            {
                Coord coord = new Coord
                {
                    Latitude = Math.Round(e.Latitude, 6),
                    Longitude = Math.Round(e.Longitude, 6)
                };

                ucJammerStation.SetJammerStationToPG(coord);
            }
            catch { }
        }

        

        private void SetAngleBRD(float e)
        {
            try
            {

                mainWindowViewModel.DirectionBRD = mainWindowViewModel.SetAngleBRD(e, mainWindowViewModel.DirectionTX);

                comBRD.SendSetAngle(0, (short)mainWindowViewModel.DirectionBRD.Azimuth, 0);

                UpdateMapDirectionBRD();
            }
            catch { }
        }


    }
}