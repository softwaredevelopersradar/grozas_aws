﻿using System;
using System.Windows.Data;
using UISuppressSource;

namespace GrozaS_AWS
{
    public class ModeUIJammingConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Mode t = (Mode)Enum.Parse(typeof(Mode), value.ToString());
            var s = (byte)t;

            return (ExternalMode)s;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}
