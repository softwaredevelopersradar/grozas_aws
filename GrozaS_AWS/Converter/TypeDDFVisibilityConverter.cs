﻿using System;
using System.Windows;
using System.Windows.Data;
using DllGrozaSProperties.Models;
namespace GrozaS_AWS
{

    public class TypeDDFVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return Binding.DoNothing;



            Visibility vis = (((TypeDF)value == TypeDF.ShIV) ? Visibility.Visible : Visibility.Hidden);


            bool f = true;
            return vis;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}

