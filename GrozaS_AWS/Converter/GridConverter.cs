﻿using System;
using System.Windows;
using System.Windows.Data;

namespace GrozaS_AWS
{

public class DoubleGridLengthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null || !(value is double))
                return Binding.DoNothing;

            return new GridLength((double)value);

        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            GridLength gridLength = (GridLength)value;
            return gridLength.Value;
        }
    }
}

