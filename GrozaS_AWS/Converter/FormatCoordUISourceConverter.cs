﻿using DllGrozaSProperties.Models;
using System;
using System.Windows.Data;

namespace GrozaS_AWS
{
    class FormatCoordUISourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null || !(value is ViewCoord))
                return Binding.DoNothing;


            CoordFormat gg = (CoordFormat)value;
            return (CoordFormat)value;


        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}
