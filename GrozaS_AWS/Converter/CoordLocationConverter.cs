﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using WpfMapControl;

namespace GrozaS_AWS
{
    public class CoordLocationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null || !(value is Coord))
                return Binding.DoNothing;

            Coord coord = (Coord)value;
            Location location = new Location(coord.Longitude, coord.Latitude);

            return location;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Location location = (Location)value;
            Coord coord = new Coord() { Latitude = location.Latitude, Longitude=location.Longitude, Altitude = 0};


            return coord;
        }
    }
}

