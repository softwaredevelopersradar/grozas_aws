﻿using DllGrozaSProperties.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace GrozaS_AWS
{
    public class ZorkiRClassConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            

            if (value == null || !(value is ClassZorkiR))
                return Binding.DoNothing;

            return (ViewZorkiR)value;

            
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}
