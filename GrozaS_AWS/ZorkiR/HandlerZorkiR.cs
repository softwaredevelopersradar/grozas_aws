﻿using Bearing;
using GrozaS_AWS.Models;
using GrozaSModelsDBLib;
using OEM;
using RadarZorkiRControl;
using Rodnik;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using WPFControlConnection;
using ZorkiR;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        System.Timers.Timer tmrZorkiRCheck = new System.Timers.Timer();

        private void ZorkiRControlConnection_ButServerClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (udpZorkiR != null && udpZorkiR.IsConnected)
                    DisconnectZorkiR();
                else
                    ConnectZorkiR();
            }

            catch
            { }
        }
        

        private void ConnectPortZorkiR(object sender, EventArgs e)
        {
            mainWindowViewModel.StateConnectionZorkiR = ConnectionStates.Connected;
            InitializeTimerZorkiRCheck();
        }

        private void DisconnectPortZorkiR(object sender, EventArgs e)
        {
            mainWindowViewModel.StateConnectionZorkiR = ConnectionStates.Disconnected;
            DestructorTimerZorkiRCheck();
        }

        private void UdpZorkiR_OnReceiveMarkPOI(object sender, MarkPOIEventArgs e)
        {
            try
            {
                //ZorkiRMark mark = DecodeMarkZorkiR(e);

                //if (mark != null)
                //{
                //    //if (mark.Flag == FlagRodnik.Reset)
                //    //    DeleteRodnikTarget(mark.ID);
                //    //else
                //        if (zorkiRTargets.Where(x => x.ID == mark.ID).ToList().Count == 0)
                //        zorkiRTargets.Add(new ZorkiRTarget { ID = mark.ID });

                //    zorkiRTargets.Where(x => x.ID == mark.ID).FirstOrDefault().zorkiRMarks.Add(mark);

                //    UpdateZorkiRTargetsMap(zorkiRTargets);


                //    UpdateTableZorkiR();
                //}


            }
            catch
            { }
        }


       

        private void UdpZorkiR_OnReceiveMarkVOI(object sender, MarkVOIEventArgs e)
        {
            try
            {
                ZorkiRMark mark = DecodeMarkZorkiR(e);

                if (mark != null)
                {
                    if (mark.State == StateZorkiR.Reset)
                        DeleteZorkiRTarget(mark.ID);
                    else
                    {
                        if (zorkiRTargets.Where(x => x.ID == mark.ID).ToList().Count == 0)
                            zorkiRTargets.Add(new ZorkiRTarget { ID = mark.ID });

                        zorkiRTargets.Where(x => x.ID == mark.ID).FirstOrDefault().zorkiRMarks.Add(mark);
                    }                        

                    UpdateZorkiRTargetsMap(zorkiRTargets);

                    UpdateTableZorkiR();
                }
            }
            catch
            {
                
            }

        }

        private ZorkiRMark DecodeMarkZorkiR(MarkVOIEventArgs e)
        {
            ZorkiRMark zorkiR = null;

            try
            {
                zorkiR = new ZorkiRMark();


                zorkiR.StartPosition = new GrozaSModelsDBLib.Coord()
                {
                    Latitude = mainWindowViewModel.LocalPropertiesVM.Lemt.Latitude,
                    Longitude = mainWindowViewModel.LocalPropertiesVM.Lemt.Longitude
                };


                zorkiR.ID = (short)e.TrackNumber;
                zorkiR.Azimuth = (float)e.Azimuth;
                zorkiR.Range = (float)e.Range;
                zorkiR.Speed = (float)e.GroundSpeed;
                zorkiR.RadialSpeed = (float)e.RadialSpeed;
                zorkiR.Aspect = (float)e.Aspect;
               

                DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                zorkiR.Loctime = start.AddMilliseconds(e.RegTime).ToLocalTime();


                zorkiR.State = (StateZorkiR)e.State;
                zorkiR.Class = (ClassZorkiR)e.TargetType;


                //Random rnd = new Random();
                //zorkiR.Class = (ClassZorkiR)rnd.Next(1, 7);
                //zorkiR.Class = ClassZorkiR.UAV;

            }
            catch { }

            return zorkiR;
        }

        private void DeleteZorkiRTarget(short ID)
        {
            try
            {
                zorkiRTargets.Remove(zorkiRTargets.Where(x => x.ID == ID).FirstOrDefault());
            }
            catch { }
        }

        private void ClearZorkiRTarget()
        {
            try
            {
                zorkiRTargets.Clear();
            }
            catch { }
        }


        private void UpdateTableZorkiR()
        {
            List<ZorkiRModel> listZorkiRModels = new List<ZorkiRModel>();


            foreach (var zor in zorkiRTargets)
            {
                listZorkiRModels.Add(new ZorkiRModel()
                {
                    Id = zor.ID,
                    Azimuth = zor.zorkiRMarks.Last().Azimuth,
                    Range = zor.zorkiRMarks.Last().Range,
                    Aspect = zor.zorkiRMarks.Last().Aspect,
                    TimeReg = zor.zorkiRMarks.Last().Loctime,
                    TargetType = Enum.GetName(typeof(ClassZorkiR), zor.zorkiRMarks.Last().Class),
                    Speed = zor.zorkiRMarks.Last().Speed,
                    RadialSpeed = zor.zorkiRMarks.Last().RadialSpeed,
                    State = Enum.GetName(typeof(StateZorkiR), zor.zorkiRMarks.Last().State),

                });
            }

            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    ucZorkiR.ListZorkiRModel = listZorkiRModels;
                }), DispatcherPriority.Background);
            }
            catch
            { }



        }


        private void InitializeTimerZorkiRCheck()
        {
            tmrZorkiRCheck.Elapsed += TickTimerZorkiRCheck;
            tmrZorkiRCheck.Interval = 3000;

            tmrZorkiRCheck.AutoReset = true;
            tmrZorkiRCheck.Enabled = true;
        }

        private void DestructorTimerZorkiRCheck()
        {
            tmrZorkiRCheck.Elapsed -= TickTimerZorkiRCheck;

            tmrZorkiRCheck.Enabled = false;
        }

        void TickTimerZorkiRCheck(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (zorkiRTargets != null)
                {
                    DateTime dateTimeNow = new DateTime();
                    dateTimeNow = DateTime.Now;

                    foreach (var t in zorkiRTargets)
                        if (dateTimeNow.Subtract(t.zorkiRMarks.Last().Loctime).TotalSeconds > mainWindowViewModel.LocalPropertiesVM.Lemt.LifeTime)
                        {
                            DeleteZorkiRTarget(t.ID);
                            UpdateZorkiRTargetsMap(zorkiRTargets);
                            UpdateTableZorkiR();
                        }
                            

                }    
            }
            catch { }

        }


        private void ucZorkiR_OnDeleteRecord(object sender, RadarZorkiRControl.ZorkiRModel e)
        {
            DeleteZorkiRTarget(e.Id);
            UpdateTableZorkiR();

            UpdateZorkiRTargetsMap(zorkiRTargets);
        }

        private void ucZorkiR_OnClearRecords(object sender, EventArgs e)
        {
            ClearZorkiRTarget();

            UpdateTableZorkiR();

            UpdateZorkiRTargetsMap(zorkiRTargets);
        }

        private void ucZorkiR_OnCentering(object sender, RadarZorkiRControl.ZorkiRModel e)
        {
            Coord coordCenter = zorkiRTargets.Where(x => x.ID == e.Id).FirstOrDefault().zorkiRMarks.Last().Coordinate;

            mainWindowViewModel.LocationCenter = coordCenter;
        }
    }
}
