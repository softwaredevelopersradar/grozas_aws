﻿using OEM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using GrozaS_AWS;

using System.Collections.ObjectModel;
using GrozaS_AWS.Models;
using ZorkiR;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        public UDPZorkiR udpZorkiR;

        ObservableCollection<ZorkiRTarget> zorkiRTargets = new ObservableCollection<ZorkiRTarget>();

        private void ConnectZorkiR()
        {
            if (udpZorkiR != null)
                DisconnectRodnik();

            udpZorkiR = new UDPZorkiR();

            udpZorkiR.OnConnectPort += ConnectPortZorkiR;
            udpZorkiR.OnDisconnectPort += DisconnectPortZorkiR;

            udpZorkiR.OnReceiveMarkPOI += UdpZorkiR_OnReceiveMarkPOI;
            udpZorkiR.OnReceiveMarkVOI += UdpZorkiR_OnReceiveMarkVOI;

            udpZorkiR.Connect(mainWindowViewModel.LocalPropertiesVM.Lemt.IpAddressLocal, mainWindowViewModel.LocalPropertiesVM.Lemt.PortLocal,
                                   mainWindowViewModel.LocalPropertiesVM.Lemt.IpAddressRemoute, mainWindowViewModel.LocalPropertiesVM.Lemt.PortRemoute);


            ClearZorkiTR();

        }

        

        private void DisconnectZorkiR()
        {

            if (udpZorkiR != null)
            {
                udpZorkiR.Disconnect();

                udpZorkiR.OnConnectPort -= ConnectPortZorkiR;
                udpZorkiR.OnDisconnectPort -= DisconnectPortZorkiR;

                udpZorkiR.OnReceiveMarkPOI -= UdpZorkiR_OnReceiveMarkPOI;
                udpZorkiR.OnReceiveMarkVOI -= UdpZorkiR_OnReceiveMarkVOI;


                udpZorkiR = null;

                ClearZorkiTR();
            }

        }

        private void ClearZorkiTR()
        {
            try 
            {
                zorkiRTargets.Clear();
                //UpdateZorkiRTargetsMap(zorkiRTargets);
            }
            catch { }
        }

    }
}

